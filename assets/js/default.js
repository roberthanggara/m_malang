$(document).ready(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    jQuery(document).ajaxStart(function () {
        $('.content-wrapper').loading({
            message: 'Proses...'
        });
    });
    jQuery(document).ajaxStop(function () {
        $('.content-wrapper').loading('stop');
    });
    $('[data-toggle="offcanvas"]').click(function () {
        $('#side-menu').toggleClass('hidden-xs');
    });
});

function fill_select_master(master, base_url, select_id, datatables, sub, no_select2) {
    var api_url = base_url + 'api/master/';
    switch (master) {
        case 'hubungankeluarga':
            api_url += 'hubungan-keluarga';
            break;
        case 'jeniskelamin':
            api_url += 'jenis-kelamin';
            break;
        case 'kecamatan':
            api_url += 'kecamatan';
            break;
        case 'kelurahan':
            api_url += 'kelurahan';
            break;
        case 'pendidikan':
            api_url += 'pendidikan';
            break;
        case 'skpd':
            api_url += 'skpd';
            break;
        case 'statusindividu':
            api_url += 'status-individu';
            break;
        case 'statuskawin':
            api_url += 'status-kawin';
            break;
        case 'statuskeluarga':
            api_url += 'status-keluarga';
            break;
        case 'statuskis':
            api_url += 'status-kis';
            break;
        default :
            sweetAlert("Gagal", "Terjadi Kesalahan", "error");
            break;
    }

    return $.ajax({
        url: api_url,
        type: "GET"
    }).done(function (result) {
        var data = jQuery.parseJSON(JSON.stringify(result));
        if (data.success) {
            if (datatables) {
                if (sub) {
                    $('#sub-modal-edit select').css('width', '100%');
                } else {
                    $('#modal-edit select').css('width', '100%');
                }
                for (var i = 0; i < select_id.length; i++) {
                    select_id[i].empty();
                    if (!no_select2) {
                        if (sub) {
                            select_id[i].select2({
                                dropdownParent: $('#sub-modal-edit')
                            });
                        } else {
                            select_id[i].select2({
                                dropdownParent: $('#modal-edit')
                            });
                        }
                    }
                }
            } else {
                if (sub) {
                    $('#sub-modal-add select').css('width', '100%');
                } else {
                    $('#modal-add select').css('width', '100%');
                }
                for (var i = 0; i < select_id.length; i++) {
                    select_id[i].empty();
                    if (!no_select2) {
                        if (sub) {
                            select_id[i].select2({
                                dropdownParent: $('#sub-modal-add')
                            });
                        } else {
                            select_id[i].select2({
                                dropdownParent: $('#modal-add')
                            });
                        }
                    }
                }
            }
            for (var x = 0; x < data.data.length; x++) {
                switch (master) {
                    case 'hubungankeluarga':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idhubungankeluarga).text(data.data[x].hubungankeluarga));
                        }
                        break;
                    case 'jeniskelamin':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idjeniskelamin).text(data.data[x].jeniskelamin));
                        }
                        break;
                    case 'kecamatan':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idkecamatan).text(data.data[x].kecamatan));
                        }
                        break;
                    case 'kelurahan':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idkelurahan).text(data.data[x].kelurahan));
                        }
                        break;
                    case 'pendidikan':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idpendidikan).text(data.data[x].pendidikan));
                        }
                        break;
                    case 'skpd':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idskpd).text(data.data[x].skpd));
                        }
                        break;
                    case 'statusindividu':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idstatusindividu).text(data.data[x].statusindividu));
                        }
                        break;
                    case 'statuskawin':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idstatuskawin).text(data.data[x].status));
                        }
                        break;
                    case 'statuskeluarga':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idstatuskeluarga).text(data.data[x].statuskeluarga));
                        }
                        break;
                    case 'statuskis':
                        for (var i = 0; i < select_id.length; i++) {
                            select_id[i].append($("<option />").val(data.data[x].idstatuskis).text(data.data[x].status));
                        }
                        break;
                    default :
                        sweetAlert("Gagal", "Terjadi Kesalahan", "error");
                        break;
                }
            }
            if (datatables) {
                switch (master) {
                    case 'hubungankeluarga':
                        $('#idhubungankeluarga').val(datatables.idhubungankeluarga);
                        break;
                    case 'jeniskelamin':
                        $('#idjeniskelamin').val(datatables.idjeniskelamin);
                        break;
                    case 'kecamatan':
                        $('#idkecamatan').val(datatables.idkecamatan);
                        break;
                    case 'kelurahan':
                        $('#idkelurahan').val(datatables.idkelurahan);
                        break;
                    case 'pendidikan':
                        $('#idpendidikan').val(datatables.idpendidikan);
                        break;
                    case 'skpd':
                        $('#idskpd').val(datatables.idskpd);
                        break;
                    case 'statusindividu':
                        $('#idstatusindividu').val(datatables.idstatusindividu);
                        break;
                    case 'statuskawin':
                        $('#idstatuskawin').val(datatables.idstatuskawin);
                        break;
                    case 'statuskeluarga':
                        $('#idstatuskeluarga').val(datatables.idstatuskeluarga);
                        break;
                    case 'statuskis':
                        $('#status_lama').val(datatables.idstatus_lama);
                        $('#status_sekarang').val(datatables.idstatus_sekarang);
                        $('#status_keterangan').val(datatables.idstatus_keterangan);
                        break;
                    default :
                        sweetAlert("Gagal", "Terjadi Kesalahan", "error");
                        break;
                }
            }
        } else {
            sweetAlert("gagal", data.message, "error");
        }
    }).fail(function (xhr, status, errorThrown) {
        sweetAlert("Gagal", "Terjadi Kesalahan", "error");
    });
}

function save_new_data(api_url, datatables, sub, xhr, formData) {
    var form_add = null;
    var ajaxProcessData = true;
    var ajaxContentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    if (sub) {
        form_add = $('.sub-form-add');
        post_data = form_add.serialize();
    } else if (formData) {
        form_add = $('form.form-add');
        post_data = new FormData(form_add[0]);
        ajaxProcessData = false;
        ajaxContentType = false;
    } else {
        form_add = $('.form-add');
        post_data = form_add.serialize();
    }

    $.ajax({
        url: api_url,
        data: post_data,
        type: "POST",
        processData: ajaxProcessData,
        contentType: ajaxContentType
    }).done(function (result) {
        var data = jQuery.parseJSON(JSON.stringify(result));
        if (data.success) {
            sweetAlert("Berhasil", data.message, "success");
            if (sub) {
                $('#sub-modal-add').modal('hide');
            } else {
                $('#modal-add').modal('hide');
            }
            form_add[0].reset();
            datatables.ajax.reload();
        } else {
            sweetAlert("gagal", data.message, "error");
        }
    }).fail(function (xhr, status, errorThrown) {
        sweetAlert("Gagal", status, "error");
    })
}

function update_data(api_url, datatables, sub, xhr, formData) {
    var form_add = null;
    var ajaxProcessData = true;
    var ajaxContentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    if (sub) {
        form_add = $('.sub-form-update');
        post_data = form_add.serialize();
    } else if (formData) {
        ajaxProcessData = false;
        ajaxContentType = false;
        form_add = $('form.form-update');
        post_data = new FormData(form_add[0]);
    } else {
        form_add = $('.form-update');
        post_data = form_add.serialize();
    }
    $.ajax({
        url: api_url,
        data: post_data,
        type: "POST",
        processData: ajaxProcessData,
        contentType: ajaxContentType,
        xhr: xhr
    }).done(function (result) {
        var data = jQuery.parseJSON(JSON.stringify(result));
        if (data.success) {
            sweetAlert("Berhasil", data.message, "success");
            if (sub) {
                $('#sub-modal-edit').modal('hide');
            } else {
                $('#modal-edit').modal('hide');
            }
            datatables.ajax.reload();
        } else {
            sweetAlert("gagal", data.message, "error");
        }
    }).fail(function (xhr, status, errorThrown) {
        sweetAlert("Gagal", "Terjadi kesalahan", "error");
    })
}

function remove_data(api_url, datatables, sub) {
    var form_edit = null;
    if (sub) {
        form_edit = $('.sub-form-edit');
    } else {
        form_edit = $('.form-edit');
    }
    swal({
            title: "Konfirmasi Hapus?",
            text: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Batal",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: api_url,
                data: form_edit.serialize(),
                type: "DELETE"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    sweetAlert("Berhasil", data.message, "success");
                    datatables.ajax.reload();
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            })
        });
}

$("#form_upload_excel").submit(function (evt) {
    evt.preventDefault();

    var url = $(this).attr('action');
    var formData = new FormData($(this)[0]);

    swal({
            title: "Konfirmasi Upload",
            text: "Apakah Anda yakin untuk mengupload file :<br><b>" + $('#excel_file').val().split("\\")[2] + "</b>",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            html: true
        },
        function () {
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    if (data.success) {
                        swal({
                            title: 'Berhasil',
                            text: data.message,
                            type: 'success'
                        }, function () {
                            location.reload();

                        });
                    } else {
                        sweetAlert("gagal", data.message, "error");
                    }
                },
                error: function (error) {
                    sweetAlert("Gagal", "Terjadi Kesalahan", "error");
                }
            }); // End: $.ajax()
        });

}); // End: submit()

function init_datatables(datatables, api_url, columns, columnDefs, priority, sub) {
    var add_col = null;
    if (sub) {
        add_col = {
            "targets": -1,
            "data": null,
            "defaultContent": "" +
            "<button class='update_btn btn btn-default btn-sm' data-toggle='modal' data-target='#sub-modal-edit'><span class='fa fa-pencil'></span></button>" +
            "<button class='remove_btn btn btn-danger btn-sm'><span class='fa fa-trash-o'></span></button>"
        };
    } else {
        add_col = {
            "targets": -1,
            "data": null,
            "defaultContent": "" +
            "<button class='update_btn btn btn-default btn-sm' data-toggle='modal' data-target='#modal-edit'><span class='fa fa-pencil'></span></button>" +
            "<button class='remove_btn btn btn-danger btn-sm'><span class='fa fa-trash-o'></span></button>"
        };
    }
    var finalColumnDefs = [
        add_col,
        {
            "responsivePriority": 1, "targets": priority
        },
        {
            "searchable": false,
            "orderable": false,
            "targets": 0
        }
    ];
    if (!columnDefs) columnDefs = [];

    finalColumnDefs = finalColumnDefs.concat(columnDefs);
    jQuery(document).ajaxStop(function () {
        $('.datatables_filter').addClass('pull-right');
        $('.buttons-colvis span').html('Seleksi Kolom');
    });
    return datatables.DataTable({
        "ajax": {
            "url": api_url,
            "type": "POST"
        },
        "deferRender": true,
        "columns": columns,
        "responsive": true,
        "autoWidth": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [[1, 'asc']],
        "columnDefs": finalColumnDefs,
        "lengthMenu": [ 10, 100, 1000, 10000, 100000, 1000000, "All"],
        dom: 'lBfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print',
            {
                extend: 'colvis',
                columns: ':not(.noVis)'
            }
        ]
    });
}

function init_datepicker(dom) {
    dom.find('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });

}

(function (deparam) {
    if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
        try {
            var jquery = require('jquery');
        } catch (e) {
        }
        module.exports = deparam(jquery);
    } else if (typeof define === 'function' && define.amd) {
        define(['jquery'], function (jquery) {
            return deparam(jquery);
        });
    } else {
        var global;
        try {
            global = (false || eval)('this'); // best cross-browser way to determine global for < ES5
        } catch (e) {
            global = window; // fails only if browser (https://developer.mozilla.org/en-US/docs/Web/Security/CSP/CSP_policy_directives)
        }
        global.deparam = deparam(global.jQuery); // assume jQuery is in global namespace
    }
})(function ($) {
    var deparam = function (params, coerce) {
        var obj = {},
            coerce_types = {'true': !0, 'false': !1, 'null': null};

        // If params is an empty string or otherwise falsy, return obj.
        if (!params) {
            return obj;
        }

        // Iterate over all name=value pairs.
        params.replace(/\+/g, ' ').split('&').forEach(function (v) {
            var param = v.split('='),
                key = decodeURIComponent(param[0]),
                val,
                cur = obj,
                i = 0,

                // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
                // into its component parts.
                keys = key.split(']['),
                keys_last = keys.length - 1;

            // If the first keys part contains [ and the last ends with ], then []
            // are correctly balanced.
            if (/\[/.test(keys[0]) && /\]$/.test(keys[keys_last])) {
                // Remove the trailing ] from the last keys part.
                keys[keys_last] = keys[keys_last].replace(/\]$/, '');

                // Split first keys part into two parts on the [ and add them back onto
                // the beginning of the keys array.
                keys = keys.shift().split('[').concat(keys);

                keys_last = keys.length - 1;
            } else {
                // Basic 'foo' style key.
                keys_last = 0;
            }

            // Are we dealing with a name=value pair, or just a name?
            if (param.length === 2) {
                val = decodeURIComponent(param[1]);

                // Coerce values.
                if (coerce) {
                    val = val && !isNaN(val) && ((+val + '') === val) ? +val        // number
                        : val === 'undefined' ? undefined         // undefined
                            : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
                                : val;                                                          // string
                }

                if (keys_last) {
                    // Complex key, build deep object structure based on a few rules:
                    // * The 'cur' pointer starts at the object top-level.
                    // * [] = array push (n is set to array length), [n] = array if n is
                    //   numeric, otherwise object.
                    // * If at the last keys part, set the value.
                    // * For each keys part, if the current level is undefined create an
                    //   object or array based on the type of the next keys part.
                    // * Move the 'cur' pointer to the next level.
                    // * Rinse & repeat.
                    for (; i <= keys_last; i++) {
                        key = keys[i] === '' ? cur.length : keys[i];
                        cur = cur[key] = i < keys_last
                            ? cur[key] || ( keys[i + 1] && isNaN(keys[i + 1]) ? {} : [] )
                            : val;
                    }

                } else {
                    // Simple key, even simpler rules, since only scalars and shallow
                    // arrays are allowed.

                    if (Object.prototype.toString.call(obj[key]) === '[object Array]') {
                        // val is already an array, so push on the next value.
                        obj[key].push(val);

                    } else if ({}.hasOwnProperty.call(obj, key)) {
                        // val isn't an array, but since a second value has been specified,
                        // convert val into an array.
                        obj[key] = [obj[key], val];

                    } else {
                        // val is a scalar.
                        obj[key] = val;
                    }
                }

            } else if (key) {
                // No value was defined, so set something meaningful.
                obj[key] = coerce
                    ? undefined
                    : '';
            }
        });

        return obj;
    };
    if ($) {
        $.prototype.deparam = $.deparam = deparam;
    }
    return deparam;
});
