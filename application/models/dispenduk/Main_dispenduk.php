<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_dispenduk extends CI_Model{
    
    public function get_data_periode_by_th($where){
    	// $this->db->wehre('left(periode, 4) =', $th);
    	$this->db->order_by('periode', 'ASC');
        $data = $this->db->get_where("dispenduk_file_data",$where)->result();
        return $data;
    }

    public function get_all_th_periode(){
    	$this->db->distinct();
		$this->db->select('left(periode, 4) as th_periode');
        $this->db->order_by('periode', 'ASC');
        $data = $this->db->get("dispenduk_file_data")->result();
        return $data;
    }

}
?>