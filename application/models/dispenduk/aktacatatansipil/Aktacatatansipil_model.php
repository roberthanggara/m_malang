<?php

class Aktacatatansipil_model extends MY_Model
{

    public $idaktacatatansipil;
    public $tanggal;
    public $idkecamatan;
    public $aktakelahiran_umum;
    public $aktakelahiran_terlambat;
    public $aktaperkawinan_bulanyanglalu;
    public $aktaperkawinan_bulanini;
    public $aktaperceraian_bulanyanglalu;
    public $aktaperceraian_bulanini;
    public $aktakematian_bulanyanglalu;
    public $aktakematian_bulanini;
    public $aktapengangkatan_bulanyanglalu;
    public $aktapengangkatan_bulanini;
    public $aktapengakuan_bulanyanglalu;
    public $aktapengakuan_bulanini;
    public $aktapengesahan_bulanyanglalu;
    public $aktapengesahan_bulanini;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dispenduk_aktacatatansipil');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dispenduk_aktacatatansipil.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dispenduk_aktacatatansipil');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dispenduk_aktacatatansipil.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dispenduk_aktacatatansipil.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idaktacatatansipil === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idaktacatatansipil', $this->idaktacatatansipil);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dispenduk_aktacatatansipil', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dispenduk_aktacatatansipil', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('aktakelahiran_umum', $this->aktakelahiran_umum);
        $this->db->set('aktakelahiran_terlambat', $this->aktakelahiran_terlambat);
        $this->db->set('aktaperkawinan_bulanyanglalu', $this->aktaperkawinan_bulanyanglalu);
        $this->db->set('aktaperkawinan_bulanini', $this->aktaperkawinan_bulanini);
        $this->db->set('aktaperceraian_bulanyanglalu', $this->aktaperceraian_bulanyanglalu);
        $this->db->set('aktaperceraian_bulanini', $this->aktaperceraian_bulanini);
        $this->db->set('aktakematian_bulanyanglalu', $this->aktakematian_bulanyanglalu);
        $this->db->set('aktakematian_bulanini', $this->aktakematian_bulanini);
        $this->db->set('aktapengangkatan_bulanyanglalu', $this->aktapengangkatan_bulanyanglalu);
        $this->db->set('aktapengangkatan_bulanini', $this->aktapengangkatan_bulanini);
        $this->db->set('aktapengakuan_bulanyanglalu', $this->aktapengakuan_bulanyanglalu);
        $this->db->set('aktapengakuan_bulanini', $this->aktapengakuan_bulanini);
        $this->db->set('aktapengesahan_bulanyanglalu', $this->aktapengesahan_bulanyanglalu);
        $this->db->set('aktapengesahan_bulanini', $this->aktapengesahan_bulanini);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idaktacatatansipil', $this->idaktacatatansipil);
        if ($this->db->update('dispenduk_aktacatatansipil')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idaktacatatansipil', $this->idaktacatatansipil);
        if ($this->db->delete('dispenduk_aktacatatansipil')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kecamatan',
            'aktakelahiran_umum',
            'aktakelahiran_terlambat',
            'aktaperkawinan_bulanyanglalu',
            'aktaperkawinan_bulanini',
            'aktaperceraian_bulanyanglalu',
            'aktaperceraian_bulanini',
            'aktakematian_bulanyanglalu',
            'aktakematian_bulanini',
            'aktapengangkatan_bulanyanglalu',
            'aktapengangkatan_bulanini',
            'aktapengakuan_bulanyanglalu',
            'aktapengakuan_bulanini',
            'aktapengesahan_bulanyanglalu',
            'aktapengesahan_bulanini',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kecamatan',
            'aktakelahiran_umum',
            'aktakelahiran_terlambat',
            'aktaperkawinan_bulanyanglalu',
            'aktaperkawinan_bulanini',
            'aktaperceraian_bulanyanglalu',
            'aktaperceraian_bulanini',
            'aktakematian_bulanyanglalu',
            'aktakematian_bulanini',
            'aktapengangkatan_bulanyanglalu',
            'aktapengangkatan_bulanini',
            'aktapengakuan_bulanyanglalu',
            'aktapengakuan_bulanini',
            'aktapengesahan_bulanyanglalu',
            'aktapengesahan_bulanini',
            'username'
        );
        $order = array('idaktacatatansipil' => 'asc'); // default order

        $this->db->select('dispenduk_aktacatatansipil.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dispenduk_aktacatatansipil');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dispenduk_aktacatatansipil.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dispenduk_aktacatatansipil.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dispenduk_aktacatatansipil');
        return $this->db->count_all_results();
    }
}
