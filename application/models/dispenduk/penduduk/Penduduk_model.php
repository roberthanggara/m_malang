<?php

class Penduduk_model extends MY_Model
{

    public $idpenduduk;
    public $tanggal;
    public $idkecamatan;
    public $jumlahkk;
    public $pendudukakhirbulanini_l;
    public $pendudukakhirbulanini_p;
    public $wajibmemilikiktp_l;
    public $wajibmemilikiktp_p;
    public $memilikiktp;
    public $belumemilikiktp;
    public $wajibmemilikiaktekelahiran;
    public $memilikiaktakelahiran;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dispenduk_penduduk');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dispenduk_penduduk.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dispenduk_penduduk');
        $this->db->join('kecamatan', 'dispenduk_penduduk.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dispenduk_penduduk.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idpenduduk === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idpenduduk', $this->idpenduduk);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dispenduk_penduduk', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dispenduk_penduduk', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('jumlahkk', $this->jumlahkk);
        $this->db->set('pendudukakhirbulanini_l', $this->pendudukakhirbulanini_l);
        $this->db->set('pendudukakhirbulanini_p', $this->pendudukakhirbulanini_p);
        $this->db->set('wajibmemilikiktp_l', $this->wajibmemilikiktp_l);
        $this->db->set('wajibmemilikiktp_p', $this->wajibmemilikiktp_p);
        $this->db->set('memilikiktp', $this->memilikiktp);
        $this->db->set('belumemilikiktp', $this->belumemilikiktp);
        $this->db->set('wajibmemilikiaktekelahiran', $this->wajibmemilikiaktekelahiran);
        $this->db->set('memilikiaktakelahiran', $this->memilikiaktakelahiran);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idpenduduk', $this->idpenduduk);
        if ($this->db->update('dispenduk_penduduk')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idpenduduk', $this->idpenduduk);
        if ($this->db->delete('dispenduk_penduduk')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'kecamatan',
            'tanggal',
            'jumlahkk',
            'pendudukakhirbulanini_l',
            'pendudukakhirbulanini_p',
            'wajibmemilikiktp_l',
            'wajibmemilikiktp_p',
            'memilikiktp',
            'belumemilikiktp',
            'wajibmemilikiaktekelahiran',
            'memilikiaktakelahiran',
            'username'
        );
        $column_search = array(
            'kecamatan',
            'tanggal',
            'jumlahkk',
            'pendudukakhirbulanini_l',
            'pendudukakhirbulanini_p',
            'wajibmemilikiktp_l',
            'wajibmemilikiktp_p',
            'memilikiktp',
            'belumemilikiktp',
            'wajibmemilikiaktekelahiran',
            'memilikiaktakelahiran',
            'username'
        );

        $order = array('idpenduduk' => 'asc'); // default order

        $this->db->select('dispenduk_penduduk.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dispenduk_penduduk');
        $this->db->join('kecamatan', 'dispenduk_penduduk.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dispenduk_penduduk.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dispenduk_penduduk');
        return $this->db->count_all_results();
    }
}
