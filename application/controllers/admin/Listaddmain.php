<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listaddmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_add_list----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "add_list_main";
		$data["list_data"] = $this->mm->get_data_all_where("add_list", array("is_delete"=>"0"));
		$this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------home_add_list----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_add_list--------------------------------
#===============================================================================
	public function val_form_insert_tipe_add_list(){
        $config_val_input = array(
                array(
                    'field'=>'keterangan',
                    'label'=>'keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_tipe_add_list(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "keterangan"=>""
                );

        if($this->val_form_insert_tipe_add_list()){
            $keterangan 	= $this->input->post("keterangan", true);

            if($this->magic_pattern->allowed_char_general($keterangan)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $is_delete  = "0";
                $insert = $this->mm->insert_data("add_list_tipe", ["keterangan"=>$keterangan, "is_delete"=>$is_delete]);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{

        	$msg_detail = array(
                    "keterangan"=>""
                );
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["keterangan"]= strip_tags(form_error('keterangan'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("add_list_tipe", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_add_list--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_tipe_add_list"])){
        	$id_tipe_add_list = $this->input->post('id_tipe_add_list', true);
        	$data = $this->mm->get_data_each("add_list_tipe", array("id_tipe_add_list"=>$id_tipe_add_list, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_add_list--------------------------------
#===============================================================================

    public function val_form_update_tipe_add_list(){
        $config_val_input = array(
                array(
                    'field'=>'keterangan',
                    'label'=>'Email',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_add_list(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "keterangan"=>""
                );

        if($this->val_form_update_tipe_add_list()){
        	$keterangan    = $this->input->post("keterangan", true);

            $id_tipe_add_list = $this->input->post("id_tipe_add_list", true);

            if($this->magic_pattern->allowed_char_general($keterangan)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $set = array(
                        "keterangan"=>$keterangan
                    );

                $where = array("id_tipe_add_list"=>$id_tipe_add_list);

                $update = $this->mm->update_data("add_list_tipe", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["keterangan"]   = strip_tags(form_error('keterangan'));
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("add_list", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_add_list--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_add_list--------------------------------
#===============================================================================

    public function delete_add_list(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_add_list"=>"",
                );

        if($_POST["id_tipe_add_list"]){
        	$id_tipe_add_list = $this->input->post("id_tipe_add_list");

            $set = array("is_delete"=>"1");
            $where = array("id_tipe_add_list"=>$id_tipe_add_list);

        	// $delete_add_list = $this->mm->delete_data("add_list", array("id_add_list"=>$id_add_list));
        	$delete_add_list = $this->mm->update_data("add_list_tipe", $set, $where);
            
            if($delete_add_list){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_add_list"]= strip_tags(form_error('id_add_list'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("add_list", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_add_list--------------------------------
#===============================================================================

}
