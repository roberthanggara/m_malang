<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/SimpleXLSX.php';

class Mainbumil extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('main/mainmodel', 'mm');

		$this->load->library("response_message");
		$this->load->library("Uploadfilev0");
	}

	public function index(){
		$data["page"] = "add_file_dispenduk";
		$data["month"] = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		// $data["data_json"] = $this->mm->get_data_all("dispenduk_file_data");

        $kecamatan = $this->db->get_where("kecamatan", [])->result();
        $array_wilayah = [];
        foreach ($kecamatan as $key => $value) {
            // print_r($value);
            $title = [
                    "id_kecamatan"=>$value->idkecamatan,
                    "kecamatan"=>$value->kecamatan
                ];

            $kelurahan = $this->db->get_where("kelurahan", ["idkecamatan"=>$value->idkecamatan])->result();

            $array_wilayah[$value->idkecamatan] = ["title"=>$title,
                                            "item"=>$kelurahan
                                        ];
        }

        // print_r($array_wilayah);
        $data["wilayah"] = json_encode($array_wilayah);

        // print_r($data["wilayah"]);
		$this->load->view('super_admin/kesehatan/form_export_bumil', $data);
	}

    public function get_excel($path, $id_file){
        $status_return = false;
        if($xlsx = SimpleXLSX::parse($path)){
            $main_data = $xlsx->rows();

            $array_status = array();

            foreach ($main_data as $key => $value) {
                // print_r($value);
                if($key >= 5){
                    if($value[0]){
                        $kelurahan  = $value[1];
                        $nama       = $value[2];
                        $umur       = $value[3];
                        $nama_suami = $value[4];
                        $alamat     = $value[5];
                        $hpht       = $value[6];
                        $tp         = $value[7];
                        $kader      = $value[8];
                        $pembiayaan = $value[9];
                        $rencana_persalinan = $value[10];
                        $keterangan = $value[11];
                        $admin = "super";
                        $date_input = date("Y-m-d H:i:s");

                        $insert = $this->db->query("SELECT insert_bumil('".$id_file."','".$kelurahan."','".$nama."', '".$umur."', '".$nama_suami."', '".$alamat."', '".$hpht."', '".$tp."', '".$kader."', '".$pembiayaan."', '".$rencana_persalinan."', '".$keterangan."', '".$date_input."', '".$admin."') AS id")->row_array();

                        $status = false;
                        if($insert){
                            // print_r($kelurahan);
                            $status = true;
                        }
                        
                        array_push($array_status, $status);
                    }
                }
            }

            if(!in_array(false, $array_status)){
                $status_return = true;
            }
        }

        return $status_return;
    }


    private function val_form_upload(){
        $config_val_input = array(
                array(
                    'field'=>'kel',
                    'label'=>'kel',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'kec',
                    'label'=>'kec',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'th',
                    'label'=>'th',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save_file(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("keterangan" => "",
                            "file" => "");

        if($this->val_form_upload()){
            $kel    = $this->input->post("kel");
            $kec    = $this->input->post("kec");
            $th     = $this->input->post("th");

            $time_input = date("Y-m-d H:i:s");

            if(isset($_FILES["file_upload"])){
                $path = './assets/main_file/kesehatan_bumil_excel/';
                $path_send = 'assets/main_file/kesehatan_bumil_excel/';
                if(!file_exists($path)){
                    mkdir($path);
                }
                // $path .= $periode.'/';
                    
                $insert = $this->db->query("select insert_bumil_excel('', '$kel', '$th') as id")->row_array();
                    if($insert){
                        $config['upload_path']          = $path;
                        $config['allowed_types']        = "xlsx";
                        $config['max_size']             = 4096;
                        $config['file_name']            = hash("sha256", $insert["id"]).".xlsx";
                           
                        $upload_data = $this->uploadfilev0->do_upload($config, "file_upload");
                        
                        if($upload_data["status"]){
                            // print_r($upload_data["main_data"]["upload_data"]["file_name"]);
                            $where = array("id"=>$insert["id"]);
                            $set = array("location_file"=>$upload_data["main_data"]["upload_data"]["file_name"]);

                            if($this->mm->update_data("kesehatan_bumil_excel", $set, $where)){
                                // print_r($insert);
                                if($this->get_excel($path_send.$upload_data["main_data"]["upload_data"]["file_name"], $insert["id"])){
                                    // print_r($path_send.$upload_data["main_data"]["upload_data"]["file_name"]);
                                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                                }
                                // ;
                            }else {
                                // print_r("gagal_update");
                                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                            }
                        }else{
                            // print_r("gagal_upload");
                            $msg_detail["file"] = $upload_data["main_msg"]["error"];
                            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                        }
                    }else{
                        // print_r("gagal_insert");
                    }
            }
        }else{
            // print_r("invalid param input");
            $msg_detail["keterangan"] = form_error("keterangan");
            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r($msg_array);

    }

	

    public function add_data(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("keterangan" => "",
    						"file" => "");

        if($this->val_form_upload()){
        	$keterangan = $this->input->post("keterangan");
        	$bln_periode = $this->input->post("bln_periode");
        	$bln_th = $this->input->post("bln_th");

            $str_periode = $bln_th.$bln_periode;
            if($bln_periode < 10){
                $str_periode = $bln_th."0".$bln_periode;    
            }
        	
        	$time_input = date("Y-m-d H:i:s");

        	if(isset($_FILES["file_upload"])){
		        $path = './assets/excel_upload/';
		        if(!file_exists($path)){
		        	mkdir($path);
		        }
		        // $path .= $periode.'/';

		        if(!$this->mm->get_data_each("dispenduk_file_data", array("periode"=>$str_periode))){
		        	$insert = $this->db->query("select insert_upload('$str_periode', '$keterangan', '$time_input') as id")->row_array();
		        	if($insert){
		        		$config['upload_path']          = $path;
				        $config['allowed_types']        = "xlsx";
				        $config['max_size']             = 4096;
				        $config['file_name']            = hash("sha256", $insert["id"]).".xlsx";
				           
				        $upload_data = $this->uploadfilev0->do_upload($config, "file_upload");
				        
				        if($upload_data["status"]){

				            $where = array("id_file_data"=>$insert["id"]);
				            $set = array("loc_file_excel"=>$upload_data["main_data"]["upload_data"]["file_name"]);

				            $set_data_for_json = $this->set_data_json($upload_data["main_data"]["upload_data"]["file_name"]);
				            if($set_data_for_json){
				            	$this->create_json(hash("sha256", $insert["id"]), json_encode($set_data_for_json));
				            	$set["loc_file_json"] = hash("sha256", $insert["id"]).".json";
				            }

				            if($this->mm->update_data("dispenduk_file_data", $set, $where)){
				            	print_r($insert);
				                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
				            }else {
				                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
				            }
				        }else{
				            $msg_detail["file"] = $upload_data["main_msg"]["error"];
				            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
				        }
		        	}
		        }else{
		        	$msg_main = array("status" => false, "msg"=>"Input data gagal, Data dengan periode ini sudah dinputkan silahkan klik update untuk mengganti");
		        }
        	}
        }else{
        	$msg_detail["keterangan"] = form_error("keterangan");
            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r($msg_array);
    }

    private function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'param',
                    'label'=>'param',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_data(){
    	$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array("param" => "");

        if($this->val_form_delete()){
        	$param = $this->input->post("param");

	    	$data = $this->mm->get_data_each("dispenduk_file_data", array("sha2(id_file_data, '512') ="=>$param));
	    	if($data){
	    		$status = true;
	    		// $path = './assets/json_upload/';
	    		if(!$this->delete_file('./assets/excel_upload/'.$data["loc_file_excel"])){
	    			$status = false;
	    		}

	    		if(!$this->delete_file('./assets/json_upload/'.$data["loc_file_json"])){
	    			$status = false;
	    		}

	    		if($status){
	    			$delete = $this->mm->delete_data("dispenduk_file_data", array("sha2(id_file_data, '512') ="=>$param));
	    			if($delete){
	    				// print_r("fine");
	    				$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
	    			}
	    		}
	    	}
        }else{
        	$msg_detail["param"] = form_error("param");
            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        }

    	$msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
        // redirect("http://localhost/dashboard_v_repair/super_admin/dispenduk/maindispenduk");
    }

    

    private function delete_file($path){
    	$status = true;
    	if(file_exists($path)){
    		$statu = $var = unlink($path) ? true : false;
    	}
    	return $status;
    }

    public function cek_data_excel(){
    	$path = 'assets/excel_upload/35a5723d31facf0db059e64797f0a437920c68b3009febd0c203770c01009c90.xlsx';

    	print_r("<pre>");
    	$this->get_lampid($path);
    }



#====================================================================================
#---------------------------------get_data_from_excel--------------------------------
#====================================================================================
    public function get_data_by_age($tgl_strart = "0", $tgl_finish = "0"){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array();

        $data = [];


        $array_wilayah  = [];
        $array_item_all = [];

        if($tgl_strart != "0" && $tgl_strart != "0"){
            $array_of_age = [   "0"=>["title"=>"< 20",
                                    "min"=>0,
                                    "max"=>20],

                                "1"=>["title"=>"21-30",
                                    "min"=>21,
                                    "max"=>30],

                                "2"=>["title"=>"31-40",
                                    "min"=>31,
                                    "max"=>40],

                                "3"=>["title"=>"41-50",
                                    "min"=>41,
                                    "max"=>50],

                                "4"=>["title"=>"51-60",
                                    "min"=>51,
                                    "max"=>60],

                                "5"=>["title"=>"> 61",
                                    "min"=>61,
                                    "max"=>100]
                        ];
            
            $kecamatan = $this->db->get_where("kecamatan", [])->result();
            $array_wilayah = [];

            $array_val_kecamatan = [];
            $array_val_kecamatan["item_sum"] = [];

            $array_item_all = [];
            
            foreach ($kecamatan as $key => $value) {
                if($key != 0){
                    $title = [
                        "id_kecamatan"=>$value->idkecamatan,
                        "kecamatan"=>$value->kecamatan
                    ];

                    $kelurahan = $this->db->get_where("kelurahan", ["idkecamatan"=>$value->idkecamatan])->result();

                    $array_wilayah[$value->idkecamatan]["title"]=$title;
                    $array_wilayah[$value->idkecamatan]["item_sum"]=[];
                    $array_wilayah[$value->idkecamatan]["item"]=[];

                    $t_all_kelurahan = 0;
                    foreach ($kelurahan as $key_kl => $value_kl) {

                        foreach ($array_of_age as $key_og => $value_og) {
                            $min_age = $value_og["min"];
                            $max_age = $value_og["max"];

                            $title = $value_og["title"];

                            if(!array_key_exists($key_og, $array_wilayah[$value->idkecamatan]["item"])){
                                $array_wilayah[$value->idkecamatan]["item"][$key_og] = ["title"=>$title, 
                                                                                        "val"=>0, 
                                                                                        ];
                            }

                            if(!array_key_exists($key_og, $array_item_all)){
                                $array_item_all[$key_og] = ["title"=>$title, 
                                                            "val"=>0];
                            }

                            
                            $tmp_sum = count($this->mm->get_data_all_where(
                                "kesehatan_bumil",
                                [   
                                    "umur>="=>$min_age,
                                    "umur<="=>$max_age,

                                    "kelurahan"=>$value_kl->idkelurahan,
                                    "if(('".$tgl_strart."' >= hptp AND '".$tgl_strart."' <= tp) OR ('".$tgl_finish."' >= hptp AND '".$tgl_finish."' <= tp), '1', if((hptp >= '".$tgl_strart."'  AND hptp <= '".$tgl_finish."') AND (tp >= '".$tgl_strart."' AND tp <= '".$tgl_finish."'), '1', '0')) ="=>"1"
                                ]
                            ));

                            $tmp_cek = $this->mm->get_data_all_where(
                                "kesehatan_bumil",
                                [   
                                    "umur>="=>$min_age,
                                    "umur<="=>$max_age,

                                    "kelurahan"=>$value_kl->idkelurahan,
                                    "if(('".$tgl_strart."' >= hptp AND '".$tgl_strart."' <= tp) OR ('".$tgl_finish."' >= hptp AND '".$tgl_finish."' <= tp), '1', if((hptp >= '".$tgl_strart."'  AND hptp <= '".$tgl_finish."') AND (tp >= '".$tgl_strart."' AND tp <= '".$tgl_finish."'), '1', '0')) ="=>"1"
                                ]
                            );
                            
                            $array_wilayah[$value->idkecamatan]["item"][$key_og]["val"] += $tmp_sum;
                            $array_item_all[$key_og]["val"] += $tmp_sum;

                        }
                    }
                }
                
            }
        }
        $data = ["wilayah"=>json_encode($array_wilayah),
                "item_all"=>json_encode($array_item_all)];

        $data["page"] = "bumil_age";

        $data["title"] = "Berdasarkan Umur Ibu";

        
        // print_r($array_item_all);
        // print_r($array_wilayah);


        // $this->load->view("kesehatan/main_bumil_age", $data);

        $this->load->view("index_mobile", $data);
    }

    public function get_data_by_hamil($tgl_strart = "0", $tgl_finish = "0"){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array();

        if($tgl_strart != "0" && $tgl_strart != "0"){
            $kecamatan = $this->db->get_where("kecamatan", [])->result();
            $array_wilayah = [];
            $array_val_kecamatan = [];
            $array_val_kecamatan["item_sum"] = [];
            foreach ($kecamatan as $key => $value) {
                if($key != 0){
                    $title = [
                        "id_kecamatan"=>$value->idkecamatan,
                        "kecamatan"=>$value->kecamatan
                    ];

                    $kelurahan = $this->db->get_where("kelurahan", ["idkecamatan"=>$value->idkecamatan])->result();

                    $array_wilayah[$value->idkecamatan]["title"]=$title;
                    $array_wilayah[$value->idkecamatan]["item_sum"]=[];

                    $t_all_kelurahan = 0;
                    foreach ($kelurahan as $key_kl => $value_kl) {
                        $array_wilayah[$value->idkecamatan]["item"][$value_kl->idkelurahan] = $value_kl;
                        $tmp_sum = count($this->mm->get_data_all_where("kesehatan_bumil",
                            ["kelurahan"=>$value_kl->idkelurahan,
                            "if(('".$tgl_strart."' >= hptp AND '".$tgl_strart."' <= tp) OR ('".$tgl_finish."' >= hptp AND '".$tgl_finish."' <= tp), '1', if((hptp >= '".$tgl_strart."'  AND hptp <= '".$tgl_finish."') AND (tp >= '".$tgl_strart."' AND tp <= '".$tgl_finish."'), '1', '0')) ="=>"1"
                            ]
                        ));


                        $array_tmp = [
                                    "title"=>$value_kl->kelurahan,
                                    "val"=>$tmp_sum
                                ];

                        $t_all_kelurahan += $tmp_sum;

                        array_push($array_wilayah[$value->idkecamatan]["item_sum"], $array_tmp);

                        // $array_wilayah[$value->idkecamatan]["item_sum"][$value->idkelurahan] = $tmp_sum;
                    }

                    $array_tmp_kec = [
                                    "title"=>$value->kecamatan,
                                    "val"=>$t_all_kelurahan
                                ];

                    array_push($array_val_kecamatan["item_sum"], $array_tmp_kec);
                }
                
            }

            $data = ["wilayah"=>json_encode($array_wilayah),
                     "kecamatan"=>json_encode($array_val_kecamatan)
                    ];
        }

        $data["page"] = "bumil_hamil";

        $data["title"] = "Berdasarkan Kehamilan";

        
        // print_r($array_val_kecamatan);
        // print_r($array_wilayah);
        // $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        // print_r($msg_array);
        // $this->load->view("kesehatan/main_bumil", $data);

        $this->load->view("index_mobile", $data);
    }

    public function get_data_by_tp($tgl_strart, $tgl_finish){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array();

        if($tgl_strart != "0" && $tgl_strart != "0"){
            $kecamatan = $this->db->get_where("kecamatan", [])->result();
            $array_wilayah = [];
            $array_val_kecamatan = [];
            $array_val_kecamatan["item_sum"] = [];
            foreach ($kecamatan as $key => $value) {
                if($key != 0){
                    $title = [
                        "id_kecamatan"=>$value->idkecamatan,
                        "kecamatan"=>$value->kecamatan
                    ];

                    $kelurahan = $this->db->get_where("kelurahan", ["idkecamatan"=>$value->idkecamatan])->result();

                    $array_wilayah[$value->idkecamatan]["title"]=$title;
                    $array_wilayah[$value->idkecamatan]["item_sum"]=[];

                    $t_all_kelurahan = 0;
                    foreach ($kelurahan as $key_kl => $value_kl) {
                        $array_wilayah[$value->idkecamatan]["item"][$value_kl->idkelurahan] = $value_kl;
                        $tmp_sum = count($this->mm->get_data_all_where("kesehatan_bumil",
                            ["kelurahan"=>$value_kl->idkelurahan,
                            "tp >="=>$tgl_strart,
                            "tp <="=>$tgl_finish
                            ]
                        ));

                        $array_tmp = [
                                    "title"=>$value_kl->kelurahan,
                                    "val"=>$tmp_sum
                                ];

                        $t_all_kelurahan += $tmp_sum;

                        array_push($array_wilayah[$value->idkecamatan]["item_sum"], $array_tmp);

                        // $array_wilayah[$value->idkecamatan]["item_sum"][$value->idkelurahan] = $tmp_sum;
                    }

                    $array_tmp_kec = [
                                    "title"=>$value->kecamatan,
                                    "val"=>$t_all_kelurahan
                                ];

                    array_push($array_val_kecamatan["item_sum"], $array_tmp_kec);
                }
                
            }

            $data = ["wilayah"=>json_encode($array_wilayah),
                     "kecamatan"=>json_encode($array_val_kecamatan)
                    ];
        }

        $data["page"] = "bumil_tp";

        $data["title"] = "Berdasarkan Tanggal Perkiraan";

        
        // print_r($array_val_kecamatan);
        // print_r($array_wilayah);
        // $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        // print_r($msg_array);
        // $this->load->view("kesehatan/main_bumil", $data);

        $this->load->view("index_mobile", $data);
    }

    public function get_data_by_hapl($tgl_strart, $tgl_finish){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array();

        if($tgl_strart != "0" && $tgl_strart != "0"){
            $kecamatan = $this->db->get_where("kecamatan", [])->result();
            $array_wilayah = [];
            $array_val_kecamatan = [];
            $array_val_kecamatan["item_sum"] = [];
            foreach ($kecamatan as $key => $value) {
                if($key != 0){
                    $title = [
                        "id_kecamatan"=>$value->idkecamatan,
                        "kecamatan"=>$value->kecamatan
                    ];

                    $kelurahan = $this->db->get_where("kelurahan", ["idkecamatan"=>$value->idkecamatan])->result();

                    $array_wilayah[$value->idkecamatan]["title"]=$title;
                    $array_wilayah[$value->idkecamatan]["item_sum"]=[];

                    $t_all_kelurahan = 0;
                    foreach ($kelurahan as $key_kl => $value_kl) {
                        $array_wilayah[$value->idkecamatan]["item"][$value_kl->idkelurahan] = $value_kl;
                        $tmp_sum = count($this->mm->get_data_all_where("kesehatan_bumil",
                            ["kelurahan"=>$value_kl->idkelurahan,
                            "hptp >="=>$tgl_strart,
                            "hptp <="=>$tgl_finish
                            ]
                        ));

                        $array_tmp = [
                                    "title"=>$value_kl->kelurahan,
                                    "val"=>$tmp_sum
                                ];

                        $t_all_kelurahan += $tmp_sum;

                        array_push($array_wilayah[$value->idkecamatan]["item_sum"], $array_tmp);

                        // $array_wilayah[$value->idkecamatan]["item_sum"][$value->idkelurahan] = $tmp_sum;
                    }

                    $array_tmp_kec = [
                                    "title"=>$value->kecamatan,
                                    "val"=>$t_all_kelurahan
                                ];

                    array_push($array_val_kecamatan["item_sum"], $array_tmp_kec);
                }
                
            }

            $data = ["wilayah"=>json_encode($array_wilayah),
                     "kecamatan"=>json_encode($array_val_kecamatan)
                    ];
        }

        $data["page"] = "bumil_hapl";

        $data["title"] = "Berdasarkan Hari Perkiraan Lahir";
        // print_r($array_val_kecamatan);
        // print_r($array_wilayah);
        // $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        // print_r($msg_array);
        // $this->load->view("kesehatan/main_bumil", $data);

        $this->load->view("index_mobile", $data);
    }

    public function get_data_by_kelurahan(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array();

        $kecamatan = $this->db->get_where("kecamatan", [])->result();
        $array_wilayah = [];
        $array_val_kecamatan = [];
        $array_val_kecamatan["item_sum"] = [];
        foreach ($kecamatan as $key => $value) {
            if($key != 0){
                $title = [
                    "id_kecamatan"=>$value->idkecamatan,
                    "kecamatan"=>$value->kecamatan
                ];

                $kelurahan = $this->db->get_where("kelurahan", ["idkecamatan"=>$value->idkecamatan])->result();

                $array_wilayah[$value->idkecamatan]["title"]=$title;
                $array_wilayah[$value->idkecamatan]["item_sum"]=[];

                $t_all_kelurahan = 0;
                foreach ($kelurahan as $key_kl => $value_kl) {
                    $array_wilayah[$value->idkecamatan]["item"][$value_kl->idkelurahan] = $value_kl;
                    $tmp_sum = count($this->mm->get_data_all_where("kesehatan_bumil", ["kelurahan"=>$value_kl->idkelurahan]));

                    $array_tmp = [
                                "title"=>$value_kl->kelurahan,
                                "val"=>$tmp_sum
                            ];

                    $t_all_kelurahan += $tmp_sum;

                    array_push($array_wilayah[$value->idkecamatan]["item_sum"], $array_tmp);

                    // $array_wilayah[$value->idkecamatan]["item_sum"][$value->idkelurahan] = $tmp_sum;
                }

                $array_tmp_kec = [
                                "title"=>$value->kecamatan,
                                "val"=>$t_all_kelurahan
                            ];

                array_push($array_val_kecamatan["item_sum"], $array_tmp_kec);
            }
        }

        $data = ["wilayah"=>json_encode($array_wilayah),
                 "kecamatan"=>json_encode($array_val_kecamatan)
                ];

        $data["page"] = "bumil_kelurahan";

        $data["title"] = "Berdasarkan Kelurahan";

        // print_r($array_val_kecamatan);
        // print_r($array_wilayah);
        // $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        // print_r($msg_array);
        // $this->load->view("kesehatan/main_bumil", $data);

        // print_r($data);
        $this->load->view("index_mobile", $data);
    }
#====================================================================================
#---------------------------------get_data_from_excel--------------------------------
#====================================================================================



}
