<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/SimpleXLSX.php';

class Pdammain extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->library("response_message");
    }

    public function index_tagihan(){
        $data["page"] = "tagihan_main";
        $this->load->view("index_mobile", $data);
    }

    public function checkTagihan(){
        $this->form_validation->set_rules("token", "token", "required");
        $this->form_validation->set_rules("nosal", "nosal", "required");
    
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(null);
    
        if($this->form_validation->run()){
            $token = $this->input->post("token");
            $nosal = $this->input->post("nosal");

    
            if($token == "X00W"){
                $url = "https://ncctrial.malangkota.go.id/dashboard_v_repair/show_report/Showpdam/checkTagihan_x";
                $fields = array(
                   'token' => 'X00W',
                   'nosal' => $nosal
                );

                $postvars = http_build_query($fields);
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, count($fields));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result = curl_exec($ch);
                curl_close($ch);

                // print_r($result);

                //print_r(json_decode($result));
                $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail = $result;

            }
        }
    
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array)); 
    }

    public function index(){
        $url = "http://114.4.37.155:8081/info/getPelanggan";
        $fields = array(
           'jenis' => 'rekap'
           // ,
           // 'status' => '0'
        );

        $postvars = http_build_query($fields);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        print_r($result);
    }

    public function index_fountainTap(){
        $url = "http://114.4.37.155:8081/info/getFountainTap";
        // $url = base_url()."assets/json_pdam/anjungan.json";

        $data_main = str_replace("(", "", file_get_contents($url));
        $data_main = str_replace(")", "", $data_main);

        $data_main = json_decode($data_main);
    

        if($data_main->success){
            $new_array = array();
            foreach ($data_main->data->detail_ft as $key => $value) {
                $sts_air = "0";
                if($value->status == "SIAP MINUM"){
                    $sts_air = "1";
                }

                $tmp_data = array(
                            "txt"=>$value->address,
                            "loc"=>[$value->latitude, $value->longitude],
                            "sts"=>$sts_air
                        );
                $new_array[$key] = $tmp_data;
            }
        }

        // print_r($new_array);
        

        $data["json_data"] = str_replace("(", "", json_encode($new_array));
        $data["json_data"] = str_replace(")", "", json_encode($data["json_data"]));
        $data["page"] = "anjungan_main";
        // print_r($data);
        $this->load->view("index_mobile", $data);
    }

    public function pelanggan_rekap(){
        $url = "http://114.4.37.155:8081/info/getPelanggan";
        
        
        $fields = array(
           'jenis' => 'rekap'
        );

        $postvars = http_build_query($fields);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        // $url = base_url()."assets/json_pdam/getPelanggan_rekap.json";
        // $result = file_get_contents($url);

        // print_r($result);
        return $result;
    }

    public function pelanggan_detail(){
        $url = "http://114.4.37.155:8081/info/getPelanggan";
        
        $fields = array(
           'jenis' => 'detail'
        );

        $postvars = http_build_query($fields);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        // $url = base_url()."assets/json_pdam/getPelanggan.json";
        // $result = file_get_contents($url);

        //print_r($result);
        return $result;
    }

    public function get_pelanggan_rekap(){
        //print_r($this->pelanggan_detail());
        
        $data_main_detail = json_decode($this->pelanggan_detail());

        $data_main_rekap = json_decode($this->pelanggan_rekap());

        $data_array = array("district"=>array(), "detail"=>array(), "all"=>array(), "periode"=>array(), "chart"=>array(), );

        if($data_main_detail->success){
            foreach ($data_main_detail->data->detail_plg as $key => $value) {
                $data_array["district"][str_replace(" ", "_", $value->kecamatan)]["kecamatan"] = $value->kecamatan;
                if(array_key_exists("val_kec", $data_array["district"][str_replace(" ", "_", $value->kecamatan)])){
                    $data_array["district"][str_replace(" ", "_", $value->kecamatan)]["val_kec"] += $value->jml;
                }else{
                    $data_array["district"][str_replace(" ", "_", $value->kecamatan)]["val_kec"] = $value->jml;
                }

                if(!array_key_exists(str_replace(" ", "_", $value->kecamatan), $data_array["detail"])){
                    $data_array["detail"][str_replace(" ", "_", $value->kecamatan)] = array();
                }

                $data_tmp = array(
                            "name"=>$value->nama_desa,
                            "value"=>$value->jml
                        );
                array_push($data_array["detail"][str_replace(" ", "_", $value->kecamatan)], $data_tmp);
                // print_r($value);
            }
        }


        if($data_main_rekap->success){
            $data_array["all"] = $data_main_rekap->data->jml_plg;
            $data_array["periode"] = $data_main_rekap->data->periode;
        }

        $data_chart = array();
        $chart_all = [];
        foreach ($data_array["district"] as $key => $value) {

            $tmp_val = $value;
            $tmp_val["subData"] = $data_array["detail"][$key];

            $tmp_all = ["name"=>$value["kecamatan"],
                        "value"=>$value["val_kec"]];

            // print_r($value);

            array_push($data_chart, $tmp_val);
            array_push($chart_all, $tmp_all);
        }

        $data_array["chart"] = json_encode($data_chart);
        $data_array["chart_all"] = json_encode($chart_all);
        $data_array["page"]  = "pelanggan_main";
        $data_array["detail"]= json_encode($data_array["detail"]);
        // print_r(json_encode($data_array));

        // print_r($data_array);

        $this->load->view("index_mobile", $data_array);
        // print_r($data_array);

        // $this->load->view("show_report/pdam/get_pelanggan", $data_array);
    }


    public function pengaduan_rekap(){
        $url = "http://114.4.37.155:8081/info/getPengaduanPublish";
        $fields = array(
           'jenis' => 'rekap'
        );

        $postvars = http_build_query($fields);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        // $url = base_url()."assets/json_pdam/getPengaduanPublish_rekap.json";

        // $result = file_get_contents($url);

        // print_r($result);
        return $result;
    }

    public function get_pengaduan_rekap(){
        // $url = "http://114.4.37.155:8081/info/getPengaduanPublish";

        $data_main_detail = json_decode($this->pengaduan_rekap());

        $data_array = array("all"=>array(), "periode"=>array(), "chart"=>array());
        $data_tmp = array();
        if($data_main_detail->success){
            $data_tmp[0] = array("name"=>"Belum di Response",
                                        "value"=>$data_main_detail->data->pengaduan[0]->blm_respon);

            $data_tmp[1] = array("name"=>"SPK",
                                        "value"=>$data_main_detail->data->pengaduan[0]->spk);

            $data_tmp[2] = array("name"=>"Telah Di Tangani",
                                        "value"=>$data_main_detail->data->pengaduan[0]->selesai);

            $data_array["periode"] = $data_main_detail->data->periode;
        }
        $data_array["all"] = json_encode($data_tmp);
        $data_array["page"] = "pengaduan_main";

        // print_r($data_array);
        // $this->load->view("show_report/pdam/get_pengaduan", $data_array);
        $this->load->view("index_mobile", $data_array);

        // print_r($data_array);
    }


    public function checkTagihan_x(){
        $this->form_validation->set_rules("token", "token", "required");
        $this->form_validation->set_rules("nosal", "nosal", "required");
    
        $msg_detail = array();
        if($this->form_validation->run()){
            $token = $this->input->post("token");
            $nosal = $this->input->post("nosal");

            // $token = "X00W";
            // $nosal = "117500";

    
            if($token == "X00W"){
                $url = 'http://114.4.37.155:8081/info/getTagihan';
                
                $fields = array(
                    'nosal' => $nosal
                );

                $postvars = http_build_query($fields);
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, count($fields));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result = curl_exec($ch);
                curl_close($ch);

                $msg_detail = $result;

            }
        }
    
        print_r($msg_detail);
    }

    public function check_tagihan_new(){
	   $this->form_validation->set_rules("token", "token", "required");
    
    	$msg_detail = array();
    	if($this->form_validation->run()){
    		$token = "X00W";
    		$nosal = $this->input->post("nosal");

    
    		if($token == "X00W"){
        		$postdata = http_build_query(
				    array(
				        'token' => 'X00W',
				        'nosal' => $nosal
				    )
				);

				$http_config = array(
					'http' => array(
				        'method'  => 'POST',
				        'header'  => 'Content-Type: application/x-www-form-urlencoded',
				        'content' => $postdata
				    ),"ssl"=>array(
		                "verify_peer"=>false,
		                "verify_peer_name"=>false,
			        )
				);

				$context  = stream_context_create($http_config);

				$result = file_get_contents('http://114.4.37.155:8081/info/getTagihan', false, $context);

				// print_r($result);

    			$msg_detail = $result;

    		}
    	}
    
    	print_r($msg_detail);		
	}

}
