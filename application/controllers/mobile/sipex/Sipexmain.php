<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sipexmain extends CI_Controller{

    public $ssl_config = array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

    public function __construct(){
        parent::__construct();

        $this->load->library("response_message");
        $this->load->library("check_connection_url");
        $this->load->library("file_library");
    }

    public function index(){
        print_r("surya");
    }

//-------------------------------Pedapatan dan Belanja---------------------------
    public function index_pendapatan_belanja(){
        $data["page"] = "sipex_pendapatan_belanja";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'pendapatan_belanja.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));

        if($status_update){
            $create_json = $this->create_json_pendapatan_belanja($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);                
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);

        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/pendapatan_belanja", $data);
        $this->load->view("index_mobile", $data);
    }

    public function create_json_pendapatan_belanja($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_pendapatan = "https://sipex.malangkota.go.id/api/datagrid1.php";
        $url_belanja    = "https://sipex.malangkota.go.id/api/datagrid2.php";

        $array_send = array();
            
        $status_url_pendapatan = false;
        if($this->check_connection_url->url_test($url_pendapatan)){
            $data_content_pendapatan = file_get_contents($url_pendapatan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_content_pendapatan)){
                $status_url_pendapatan = true;
            }
        }


        $status_url_belanja = false;
        if($this->check_connection_url->url_test($url_belanja)){
            $data_content_belanja = file_get_contents($url_belanja, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_content_belanja)){
                $status_url_belanja = true;
            }
        }

        if($status_url_pendapatan && $status_url_belanja){

            $array_send = array(
                        "pendapatan"=>json_decode($data_content_pendapatan),
                        "belanja"   =>json_decode($data_content_belanja)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------Pedapatan dan Belanja---------------------------


//-------------------------------Kelompok----------------------------------------
    public function index_kelompok(){
        $data["page"] = "sipex_kelompok";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'kelompok.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        if($status_update){
            $create_json = $this->create_json_kelompok($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/kelompok_pendapatan", $data);
        $this->load->view("index_mobile", $data);
    }

    public function create_json_kelompok($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_kelompok = "https://sipex.malangkota.go.id/api/databarkelompok.php";
        // $url_belanja    = "https://sipex.malangkota.go.id/api/datagrid2.php";

        $array_send = array();
            
        $status_url_kelompok = false;

        $data_content = [];

        if($this->check_connection_url->url_test($url_kelompok)){
            $data_content = file_get_contents($url_kelompok, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_content)){
                $status_url_kelompok = true;
            }
        }

        if($status_url_kelompok){

            $array_send = array(
                        "kelompok"=>json_decode($data_content)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------Kelompok----------------------------------------


//-------------------------------Jenis-------------------------------------------
    public function index_jenis(){
        $data["page"] = "sipex_jenis";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'jenis.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        if($status_update){
            $create_json = $this->create_json_jenis($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        // print_r(json_encode($msg_array));
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/jenis_pendapatan", $data);

        $this->load->view("index_mobile", $data);

    }

    public function create_json_jenis($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_pendapatan_asli    = "https://sipex.malangkota.go.id/api/datagrid_pendapatanjenis1.php";
        $url_dana_perimbangan   = "https://sipex.malangkota.go.id/api/datagrid_pendapatanjenis2.php";
        $url_lain_lain          = "https://sipex.malangkota.go.id/api/datagrid_pendapatanjenis3.php";
        // $url_belanja    = "https://sipex.malangkota.go.id/api/datagrid2.php";

        $array_send = array();
            
        $status_pendapatan_asli = false;
        if($this->check_connection_url->url_test($url_pendapatan_asli)){
            $data_pendapatan_asli = file_get_contents($url_pendapatan_asli, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_pendapatan_asli)){
                $status_pendapatan_asli = true;
            }
        }

        $status_dana_perimbangan = false;
        if($this->check_connection_url->url_test($url_dana_perimbangan)){
            $data_dana_perimbangan = file_get_contents($url_dana_perimbangan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_dana_perimbangan)){
                $status_dana_perimbangan = true;
            }
        }

        $status_lain_lain = false;
        if($this->check_connection_url->url_test($url_lain_lain)){
            $data_lain_lain = file_get_contents($url_lain_lain, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_lain_lain)){
                $status_lain_lain = true;
            }
        }

        if($status_pendapatan_asli && $status_dana_perimbangan && $status_lain_lain){

            $array_send = array(
                        "pendapatan_asli"=>json_decode($data_pendapatan_asli),
                        "dana_perimbangan"=>json_decode($data_dana_perimbangan),
                        "lain_lain"=>json_decode($data_lain_lain)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------Jenis-------------------------------------------


//-------------------------------object_pad--------------------------------------
    public function index_object_pad(){
        $data["page"] = "sipex_object_pad";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'object_pad.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        if($status_update){
            $create_json = $this->create_json_object_pad($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/object_pad", $data);
        $this->load->view("index_mobile", $data);
    }

    public function create_json_object_pad($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_pajak_daerah     = "https://sipex.malangkota.go.id/api/datagrid_pendapatanobjek1.php";
        $url_retribusi_daerah = "https://sipex.malangkota.go.id/api/datagrid_pendapatanobjek2.php";
        $url_kekayaan         = "https://sipex.malangkota.go.id/api/datagrid_pendapatanobjek3.php";
        $url_lain_lain        = "https://sipex.malangkota.go.id/api/datagrid_pendapatanobjek4.php";

        $array_send = array();
            
        $status_pajak_daerah = false;
        if($this->check_connection_url->url_test($url_pajak_daerah)){
            $data_pajak_daerah = file_get_contents($url_pajak_daerah, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_pajak_daerah)){
                $status_pajak_daerah = true;
            }
        }

        $status_retribusi_daerah = false;
        if($this->check_connection_url->url_test($url_retribusi_daerah)){
            $data_retribusi_daerah = file_get_contents($url_retribusi_daerah, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_retribusi_daerah)){
                $status_retribusi_daerah = true;
            }
        }

        $status_kekayaan = false;
        if($this->check_connection_url->url_test($url_kekayaan)){
            $data_kekayaan = file_get_contents($url_kekayaan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_kekayaan)){
                $status_kekayaan = true;
            }
        }

        $status_lain_lain = false;
        if($this->check_connection_url->url_test($url_lain_lain)){
            $data_lain_lain = file_get_contents($url_lain_lain, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_lain_lain)){
                $status_lain_lain = true;
            }
        }

        if($status_pajak_daerah && $status_retribusi_daerah && $status_kekayaan && $status_lain_lain){

            $array_send = array(
                        "pajak_daerah"=>json_decode($data_pajak_daerah),
                        "retribusi_daerah"=>json_decode($data_retribusi_daerah),
                        "kekayaan"=>json_decode($data_kekayaan),
                        "lain_lain"=>json_decode($data_lain_lain)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------object_pad--------------------------------------


//-------------------------------rincian_object----------------------------------
    public function index_rincian_object(){
        $data["page"] = "sipex_rincian_object";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'rincian_object.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        if($status_update){
            $create_json = $this->create_json_rincian_object($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/rincian_object", $data);
        $this->load->view("index_mobile", $data);
    }

    public function create_json_rincian_object($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_pajak_daerah     = "https://sipex.malangkota.go.id/api/datagrid_pendapatanrinci1.php?urutkan=paguZ";
        $url_retribusi_daerah = "https://sipex.malangkota.go.id/api/datagrid_pendapatanrinci2.php?urutkan=paguZ";
        $url_kekayaan         = "https://sipex.malangkota.go.id/api/datagrid_pendapatanrinci3.php?urutkan=paguZ";
        $url_lain_lain        = "https://sipex.malangkota.go.id/api/datagrid_pendapatanrinci4.php?urutkan=paguZ";

        $array_send = array();
            
        $status_pajak_daerah = false;
        if($this->check_connection_url->url_test($url_pajak_daerah)){
            $data_pajak_daerah = file_get_contents($url_pajak_daerah, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_pajak_daerah)){
                $status_pajak_daerah = true;
            }
        }

        $status_retribusi_daerah = false;
        if($this->check_connection_url->url_test($url_retribusi_daerah)){
            $data_retribusi_daerah = file_get_contents($url_retribusi_daerah, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_retribusi_daerah)){
                $status_retribusi_daerah = true;
            }
        }

        $status_kekayaan = false;
        if($this->check_connection_url->url_test($url_kekayaan)){
            $data_kekayaan = file_get_contents($url_kekayaan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_kekayaan)){
                $status_kekayaan = true;
            }
        }

        $status_lain_lain = false;
        if($this->check_connection_url->url_test($url_lain_lain)){
            $data_lain_lain = file_get_contents($url_lain_lain, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_lain_lain)){
                $status_lain_lain = true;
            }
        }

        if($status_pajak_daerah && $status_retribusi_daerah && $status_kekayaan && $status_lain_lain){

            $array_send = array(
                        "pajak_daerah"=>json_decode($data_pajak_daerah),
                        "retribusi_daerah"=>json_decode($data_retribusi_daerah),
                        "kekayaan"=>json_decode($data_kekayaan),
                        "lain_lain"=>json_decode($data_lain_lain)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------rincian_object----------------------------------


//-------------------------------realisasi_pendapatan----------------------------
    public function index_realisasi_pendapatan(){
        $data["page"] = "sipex_realisasi_pendapatan";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'realisasi_pendapatan.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        if($status_update){
            $create_json = $this->create_json_realisasi_pendapatan($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/realisasi_pendapatan", $data);

        $this->load->view("index_mobile", $data);
    }

    public function create_json_realisasi_pendapatan($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_realisasi_pendapatan = "https://sipex.malangkota.go.id/api/datagrid_pendapatanrealisasi.php?urutkan=paguZ";
        // $url_belanja    = "https://sipex.malangkota.go.id/api/datagrid2.php";

        $array_send = array();
            
        $status_url_realisasi_pendapatan = false;
        if($this->check_connection_url->url_test($url_realisasi_pendapatan)){
            $data_realisasi_pendapatan = file_get_contents($url_realisasi_pendapatan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_realisasi_pendapatan)){
                $status_url_realisasi_pendapatan = true;
            }
        }

        if($status_url_realisasi_pendapatan){

            $array_send = array(
                        "realisasi_pendapatan"=>json_decode($data_realisasi_pendapatan)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------realisasi_pendapatan----------------------------


//-------------------------------belanja_kelompok--------------------------------
    public function index_belanja_kelompok(){
        $data["page"] = "sipex_belanja_kelompok";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'belanja_kelompok.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));

        if($status_update){
            $create_json = $this->create_json_belanja_kelompok($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);                
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/belanja_kelompok", $data);

        $this->load->view("index_mobile", $data);
    }

    public function create_json_belanja_kelompok($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_belanja_langsung       = "https://sipex.malangkota.go.id/api/datagrid_belanja1.php";
        $url_belanja_tak_langsung   = "https://sipex.malangkota.go.id/api/datagrid_belanja2.php";

        $array_send = array();
            
        $status_url_belanja_langsung = false;
        if($this->check_connection_url->url_test($url_belanja_langsung)){
            $data_belanja_langsung = file_get_contents($url_belanja_langsung, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_langsung)){
                $status_url_belanja_langsung = true;
            }
        }


        $status_url_belanja_tak_langsung = false;
        if($this->check_connection_url->url_test($url_belanja_tak_langsung)){
            $data_belanja_tak_langsung = file_get_contents($url_belanja_tak_langsung, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_tak_langsung)){
                $status_url_belanja_tak_langsung = true;
            }
        }

        if($status_url_belanja_langsung && $status_url_belanja_tak_langsung){

            $array_send = array(
                        "belanja_langsung"      =>json_decode($data_belanja_langsung),
                        "belanja_tak_langsung"  =>json_decode($data_belanja_tak_langsung)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------belanja_kelompok--------------------------------


//-------------------------------belanja_jenis-----------------------------------
    public function index_belanja_jenis(){
        $data["page"] = "sipex_belanja_jenis";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'belanja_jenis.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));

        if($status_update){
            $create_json = $this->create_json_belanja_jenis($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);                
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/belanja_jenis", $data);

        $this->load->view("index_mobile", $data);
    }

    public function create_json_belanja_jenis($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_belanja_langsung       = "https://sipex.malangkota.go.id/api/datagrid_belanjajenis1.php";
        $url_belanja_tak_langsung   = "https://sipex.malangkota.go.id/api/datagrid_belanjajenis2.php";

        $array_send = array();
            
        $status_url_belanja_langsung = false;
        if($this->check_connection_url->url_test($url_belanja_langsung)){
            $data_belanja_langsung = file_get_contents($url_belanja_langsung, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_langsung)){
                $status_url_belanja_langsung = true;
            }
        }


        $status_url_belanja_tak_langsung = false;
        if($this->check_connection_url->url_test($url_belanja_tak_langsung)){
            $data_belanja_tak_langsung = file_get_contents($url_belanja_tak_langsung, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_tak_langsung)){
                $status_url_belanja_tak_langsung = true;
            }
        }

        if($status_url_belanja_langsung && $status_url_belanja_tak_langsung){

            $array_send = array(
                        "belanja_langsung"      =>json_decode($data_belanja_langsung),
                        "belanja_tak_langsung"  =>json_decode($data_belanja_tak_langsung)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------belanja_jenis-----------------------------------


//-------------------------------belanja_opd-------------------------------------
    public function index_belanja_opd(){
        $data["page"] = "sipex_belanja_opd";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'belanja_opd.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));

        if($status_update){

            // print_r("true");
            $create_json = $this->create_json_belanja_opd($dir, $file_name);
            if($create_json){
                // print_r("ok");
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);                
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/belanja_opd", $data);

        $this->load->view("index_mobile", $data);
    }

    public function create_json_belanja_opd($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_belanja_opd       = "https://sipex.malangkota.go.id/api/datagrid_belanjarealisasi1.php";
        $url_belanja_kelurahan = "https://sipex.malangkota.go.id/api/datagrid_belanjarealisasi2.php";
        $url_belanja_sekolah   = "https://sipex.malangkota.go.id/api/datagrid_belanjarealisasi3.php";
        $url_belanja_bagian    = "https://sipex.malangkota.go.id/api/datagrid_belanjarealisasi4.php";

        $array_send = array();
            
        $status_url_belanja_opd = false;
        if($this->check_connection_url->url_test($url_belanja_opd)){
            $data_belanja_opd = file_get_contents($url_belanja_opd, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_opd)){
                $status_url_belanja_opd = true;
                // print_r("status_url_belanja_opd<br>");
            }
        }


        $status_url_belanja_kelurahan = false;
        if($this->check_connection_url->url_test($url_belanja_kelurahan)){
            $data_belanja_kelurahan = file_get_contents($url_belanja_kelurahan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_kelurahan)){
                $status_url_belanja_kelurahan = true;
                // print_r("status_url_belanja_kelurahan<br>");
            }
        }


        $status_url_belanja_sekolah = false;
        if($this->check_connection_url->url_test($url_belanja_sekolah)){
            $data_belanja_sekolah = file_get_contents($url_belanja_sekolah, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_sekolah)){
                $status_url_belanja_sekolah = true;
                // print_r("status_url_belanja_sekolah<br>");
            }
        }


        $status_url_belanja_bagian = false;
        if($this->check_connection_url->url_test($url_belanja_bagian)){
            $data_belanja_bagian = file_get_contents($url_belanja_bagian, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_belanja_bagian)){
                $status_url_belanja_bagian = true;
                // print_r("status_url_belanja_bagian<br>");
            }
        }


        if($status_url_belanja_opd && $status_url_belanja_kelurahan && $status_url_belanja_sekolah && $status_url_belanja_bagian){

            $array_send = array(
                        "belanja_opd"       =>json_decode($data_belanja_opd),
                        "belanja_kelurahan" =>json_decode($data_belanja_kelurahan),
                        "belanja_sekolah"   =>json_decode($data_belanja_sekolah),
                        "belanja_bagian"    =>json_decode($data_belanja_bagian)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }


        return $status_send;
    }
//-------------------------------belanja_opd-------------------------------------


//-------------------------------Pembiayaan--------------------------------------
    public function index_pembiayaan(){
        $data["page"] = "sipex_pembiayaan";

        $str_date_time_now = date("Y-m-d H:i:s");

        $file_name  = 'pembiayaan.json';
        $dir        = './assets/json_tmp/';

        $str_content = $this->file_library->read_json($dir, $file_name);
        $data_local_json = json_decode($str_content);

        // print_r($data_local_json);
        $status_update = false;

        if($data_local_json){
            // print_r("ok");
            if(array_key_exists("last_update", $data_local_json)){
                $str_date_time = $data_local_json->last_update;
                if($this->check_connection_url->check_time_update($str_date_time)){
                    $status_update = true;  
                }
            }else{
                $status_update = true;
            }
            
            // print_r($str_date_time);
        }else {
            $status_update = true;
        }

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));

        if($status_update){
            $create_json = $this->create_json_pembiayaan($dir, $file_name);
            if($create_json){
                $str_content = $this->file_library->read_json($dir, $file_name);
                $data_local_json = json_decode($str_content);                
            }  
        }

        if($data_local_json != null){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_detail = array("response"=> $data_local_json);
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);

        $data["list_data"] = json_encode($msg_array);
        // print_r(json_encode($msg_array));
        // $this->load->view("show_report/sipex/pembiayaan", $data);

        $this->load->view("index_mobile", $data);
    }

    public function create_json_pembiayaan($dir, $file_name){
        $status_send = false;
        date_default_timezone_set("Asia/Bangkok");

        $url_penerimaan = "https://sipex.malangkota.go.id/api/datagrid_pembiayaan1.php";
        $url_pengeluaran    = "https://sipex.malangkota.go.id/api/datagrid_pembiayaan2.php";

        $array_send = array();
            
        $status_url_penerimaan = false;
        if($this->check_connection_url->url_test($url_penerimaan)){
            $data_content_penerimaan = file_get_contents($url_penerimaan, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_content_penerimaan)){
                $status_url_penerimaan = true;
            }
        }


        $status_url_pengeluaran = false;
        if($this->check_connection_url->url_test($url_pengeluaran)){
            $data_content_pengeluaran = file_get_contents($url_pengeluaran, false, stream_context_create($this->ssl_config));
            if($this->file_library->is_JSON($data_content_pengeluaran)){
                $status_url_pengeluaran = true;
            }
        }

        if($status_url_penerimaan && $status_url_pengeluaran){

            $array_send = array(
                        "penerimaan"=>json_decode($data_content_penerimaan),
                        "pengeluaran"   =>json_decode($data_content_pengeluaran)
                    );

            $array_response = array(
                        "main_item"=>$array_send,
                        "last_update"=>date('Y-m-d H:i:s')
                    );

            if($this->file_library->create_json($dir, $file_name, json_encode($array_response))){
                $status_send = true;
            }
             
        }

	//print_r($array_send);


        return $status_send;
    }
//-------------------------------Pembiayaan--------------------------------------


    public function check_json(){
        // $file_name  = 'rincian_object.json';
        // $dir        = './assets/json_tmp/';

        // $str_content = $this->file_library->read_json($dir, $file_name);
        // $data_local_json = json_decode($str_content);

        // print_r($data_local_json);

        // if($data_local_json != null){
        //     print_r("ada");
        // }else {
        //     print_r("tidak ada");
        // }
        // $this->load->view("show_report/sipex/coba");
        $url_kelompok = "https://sipex.malangkota.go.id/api/databarkelompok.php";
        if($this->check_connection_url->url_test($url_kelompok)){
            print_r("url ok");
            $data_content = file_get_contents($url_kelompok, false, stream_context_create($this->ssl_config));
            print_r($data_content);
        }else {
            print_r("asu");
        }
    }
}
