<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suratmiskinmain extends CI_Controller{

    public function __construct()
    {
        parent::__construct();


        // $this->load->library('form_validation');
        // $this->load->library('Generate_json');
        // $this->load->library('Generate_id');
        // $this->load->library('auth');
        // $this->load->model('katalog/katalog_model', 'katalog_db');


        // $this->submodule = 'katalog';
    }

    public $ssl_config = array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
    
    #add new by surya ncc
    #get api from SPM
        #->sebaran rumah sakit
        #->sebaran penyakit
        #->sebaran pengajuan PBI
        #->sebaran pengajuan SPM
        #->sebaran pengajuan SPM di terima
    
    public function index(){
        // $this->load->view('chart/show_spm');
        $data["page"] = "spm_main";

        $array_month = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        
        $array_data_new = array(
                        "rs_data"       =>array(
                                            "title" => "Sebaran Rumah Sakit",
                                            "item"  => array()
                                        ),
                        "penyakit_data" =>array(
                                            "title" => "Sebaran Pasien Rujukan Penyakit SPM",
                                            "item"  => array()
                                        ),
                        "dinsos_data"   =>array(
                                            "title" => "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)",
                                            "item"  => array()
                                        ),
                        "kel_data"      =>array(
                                            "title" => "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)",
                                            "item"  => array()
                                        ),
                        "verif_data"    =>array(
                                            "title" => "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)",
                                            "item"  => array()
                                        ),
                    );
        
        $array_status = array();


        $status = false;
        if($this->get_index("rs_data")){
            $rs_data = json_decode($this->get_index("rs_data"));
            $tmp_array = array();
            foreach ($rs_data as $key => $value) {
                $tmp = array(
                    "keterangan"=>$value->name,
                    "value"=>$value->data[0]
                );
                // print_r($value);

                array_push($tmp_array, $tmp);
            }

            $array_data_new["rs_data"]["item"] = $tmp_array;


            $status = true;
        }else{
            $status = false;
        }

        array_push($array_status, $status);



        $status = false;
        if($this->get_index("penyakit_data")){
            $penyakit_data = json_decode($this->get_index("penyakit_data"));
            $tmp_array = array();
            foreach ($penyakit_data as $key => $value) {
                $tmp = array(
                    "keterangan"=>$value->name,
                    "value"=>$value->data[0]
                );
                // print_r($value);

                array_push($tmp_array, $tmp);
            }

            $array_data_new["penyakit_data"]["item"] = $tmp_array;

            $status = true;
        }else{
            $status = false;
        }

        array_push($array_status, $status);
        


        $status = false;
        if($this->get_index("dinsos_data")){
            $dinsos_data = json_decode($this->get_index("dinsos_data"));
            $tmp_array = array();
            foreach ($dinsos_data as $key => $value) {
                $tmp = array(
                    "keterangan"=>$array_month[$key],
                    "value"=>$value
                );

                array_push($tmp_array, $tmp);
            }
            
            $array_data_new["dinsos_data"]["item"] = $tmp_array;

            $status = true;
        }else{
            $status = false;
        }

        array_push($array_status, $status);


        $status = false;
        if($this->get_index("kel_data")){
            $kel_data = json_decode($this->get_index("kel_data"));
            $tmp_array = array();
            foreach ($kel_data as $key => $value) {
                $tmp = array(
                    "keterangan"=>$array_month[$key],
                    "value"=>$value
                );

                array_push($tmp_array, $tmp);
            }
            
            $array_data_new["kel_data"]["item"] = $tmp_array;
            // $array_data_new["kel_data"] = $kel_data;
            
            $status = true;
        }else{
            $status = false;
        }

        array_push($array_status, $status);


        $status = false;
        if($this->get_index("verif_data")){
            $verif_data = json_decode($this->get_index("verif_data"));
            $tmp_array = array();
            foreach ($verif_data as $key => $value) {
                $tmp = array(
                    "keterangan"=>$array_month[$key],
                    "value"=>$value
                );

                array_push($tmp_array, $tmp);
            }
            
            $array_data_new["verif_data"]["item"] = $tmp_array;
            // $array_data_new["verif_data"] = $verif_data;
            
            $status = true;
        }else{
            $status = false;
        }

        array_push($array_status, $status);

        // $verif_data = json_decode($this->get_index("verif_data"));
        // print_r($verif_data);

        $data["data_json"] = json_encode(array());

        if(!in_array(false, $array_status)){
            $data["data_json"] = json_encode($array_data_new);
        }

        // print_r($data["data_json"]);

        $this->load->view("index_mobile", $data);
        // $this->load->view("show_report/spm", $data);
        
    }

    public function get_index($url){
        switch ($url) {
            case 'rs_data':
                $url_pendapatan = "https://spm.malangkota.go.id/monitoring/ws/rs_data";
                break;
            
            case 'penyakit_data':
                $url_pendapatan = "https://spm.malangkota.go.id/monitoring/ws/penyakit_data";
                break;
            
            case 'dinsos_data':
                $url_pendapatan = "https://spm.malangkota.go.id/monitoring/ws/dinsos_data";
                break;
            
            case 'kel_data':
                $url_pendapatan = "https://spm.malangkota.go.id/monitoring/ws/kel_data";
                break;
            
            case 'verif_data':
                $url_pendapatan = "https://spm.malangkota.go.id/monitoring/ws/verif_data";
                break;
            
            default:
                $url_pendapatan = "https://spm.malangkota.go.id/monitoring/ws/rs_data";
                break;
        }
        
        $data_content_pendapatan = file_get_contents($url_pendapatan, false, stream_context_create($this->ssl_config));
        // $data = json_decode($data_content_pendapatan);

        // print_r($data);

        return $data_content_pendapatan;
    }


#----------------------------------------------------------------------sebaran RS----------------------------------------------------------------------#
    public function get_sebaran_rs(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/rs_data");
        $data = json_decode($content);
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){
                echo "<tr>
                        <td>".$val_data->name."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data->data[0]."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        
        //$this->load->view('chart/show_spm');
    }
    
    public function get_rs_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/rs_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Rumah Sakit";
        $data_send["chart_js"]="var chartData = [";   
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){   
                //print_r($val_data);
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                          \"country\": \"".$val_data->name."\",
                                            \"visits\": ".$val_data->data[0].",
                                        \"color\": \"#3c8dbc\"
                                        },";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    }
#----------------------------------------------------------------------end sebaran RS----------------------------------------------------------------------#


#----------------------------------------------------------------------end sebaran Penyakit----------------------------------------------------------------------#    
    public function get_sebaran_penyakit(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/penyakit_data");
        //print_r($content);
        $data = json_decode($content);
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){
                echo "<tr>
                        <td>".$val_data->name."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data->data[0]."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        
    }
    
    public function get_penyakit_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/penyakit_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pasien Rujukan Penyakit SPM";
        $data_send["chart_js"]="var chartData = [";   
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){   
                //print_r($val_data);
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                          \"country\": \"".$val_data->name."\",
                                            \"visits\": ".$val_data->data[0].",
                                        \"color\": \"#3c8dbc\"
                                        },";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
#----------------------------------------------------------------------end sebaran Penyakit----------------------------------------------------------------------#    



#----------------------------------------------------------------------sebaran penjualan PBI----------------------------------------------------------------------#    
    
    public function get_sebaran_pbi(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/dinsos_data");
        //print_r($content);
        $data = json_decode($content);
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                echo "<tr>
                        <td>".$month[$r_val]."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        //print_r($data);
        
    }

    public function get_pbi_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/dinsos_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)";
        $data_send["chart_js"]="var chartData = [";
           
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                          \"country\": \"".$month[$r_val]."\",
                                            \"visits\": ".$val_data.",
                                        \"color\": \"#3c8dbc\"
                                        },";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
#----------------------------------------------------------------------End sebaran penjualan PBI----------------------------------------------------------------------#    



#----------------------------------------------------------------------sebaran pengajuan SPM----------------------------------------------------------------------#    

    public function get_sebaran_pengajuan_SPM(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/kel_data");
        //print_r($content);
        $data = json_decode($content);
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                echo "<tr>
                        <td>".$month[$r_val]."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        //print_r($data);
    }
    
    public function get_SPM_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/kel_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)";
        $data_send["chart_js"]="var chartData = [";
           
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                          \"country\": \"".$month[$r_val]."\",
                                            \"visits\": ".$val_data.",
                                        \"color\": \"#3c8dbc\"
                                        },";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
#----------------------------------------------------------------------End sebaran pengajuan SPM----------------------------------------------------------------------#    



#----------------------------------------------------------------------sebaran pengajuan SPM diterima----------------------------------------------------------------------#    

    public function get_sebaran_pengajuan_SPM_diterima(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/verif_data");
        //print_r($content);
        $x = "\"\"";
        $y = "\"\"";
        
        $data = json_decode($content);
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                echo "<tr>
                        <td>".$month[$r_val]."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data."</td>
                    </tr>";
                    
                $x = $x.",\"".$month[$r_val]."\"";
                $y = $y.",\"".$val_data."\"";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        
        
        //print_r($data);
    }
    
    public function get_SPM_diterima_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/verif_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)";
        $data_send["chart_js"]="var chartData = [";
           
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                          \"country\": \"".$month[$r_val]."\",
                                            \"visits\": ".$val_data.",
                                        \"color\": \"#3c8dbc\"
                                        },";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
}
