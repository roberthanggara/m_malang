<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kewilayahanmain extends CI_Controller {
    public $ssl_config = array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
    );


    public function __construct(){
        parent::__construct();
        // $this->load->library('auth');

    }

    public function kewilayahan(){
        $data_send["page"] = "kewilayahan_main";
        $data = file_get_contents(base_url()."assets/json/mlg_kel_new2.json", false, stream_context_create($this->ssl_config));
        $data_array = json_decode($data);

        $data_send["data_main"] = json_encode($data_array);

        $this->load->view("index_mobile", $data_send);
    }

}
