<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/SimpleXLSX.php';

class Homemain extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->library("response_message");
    }

    public function index(){
        $data["page"] = "home_main";

        $this->load->view("index_mobile", $data);
    }

}
