<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>adminpress/assets/images/blessindo-icon-16.png">
    <title>Mobile Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>adminpress/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?php print_r(base_url());?>adminpress/assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>adminpress/horizontal/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>adminpress/horizontal/css/colors/blue.css" id="theme" rel="stylesheet">


    <script src="<?php print_r(base_url())?>assets/amcharts4/core.js"></script>
    <script src="<?php print_r(base_url())?>assets/amcharts4/charts.js"></script>
    <script src="<?php print_r(base_url())?>assets/amcharts4/themes/dark.js"></script>
    <script src="<?php print_r(base_url())?>assets/amcharts4/themes/animated.js"></script>

    <script src="<?php echo base_url("assets/js/jquery-3.2.1.js") ?>"></script>

    <style type="text/css">
        .litle_text {
              font-size: 10px;.litle_text
            }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="<?php print_r(base_url());?>adminpress/assets/images/logo-icon.png" alt="homepage" class="dark-logo" /> -->
                            <!-- Light Logo icon -->
                            <!-- <img src="<?php print_r(base_url());?>adminpress/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" /> -->
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php print_r(base_url());?>assets/images/header_logo.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php print_r(base_url());?>assets/images/header_logo.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        
                        
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input list="list_url" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a>

                                <datalist id="list_url">
                                    <option value="Internet Explorer">
                                    <option value="Firefox">
                                    <option value="Chrome">
                                    <option value="Opera">
                                    <option value="Safari">
                                </datalist>
                            </form>
                        </li>

                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php print_r(base_url());?>adminpress/assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php print_r(base_url());?>adminpress/assets/images/users/1.jpg" alt="user"></div>
                                            <div class="u-text">
                                                <h4>Steave Jobs</h4>
                                                <p class="text-muted">varun@gmail.com</p><a href="pages-profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">DASHBOARD</li>
                        <!-- Menu Home -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>home" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a> </li>
                        <!-- Menu Pendidikan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-mortar-board"></i><span class="hide-menu">Pendidikan</span></a> 
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>data/pendidikan/main">Dashboard Data Pendidikan</a></li>
                                <li><a href="<?php print_r(base_url());?>data/pendidikan/sd">Rangkuman Data Sekolah Dasar</a></li>
                                <li><a href="<?php print_r(base_url());?>data/pendidikan/smp">Rangkuman Data Sekolah Menegah Pertama</a></li>
                                <li><a href="<?php print_r(base_url());?>data/pendidikan/sma">Rangkuman Data Sekolah Menegah Atas</a></li>
                                <li><a href="<?php print_r(base_url());?>data/pendidikan/peta_sekolah">Peta Sebaran Satuan Pendidikan (Sekolah)</a></li>
                            </ul>
                        </li>
                        
                        <!-- Menu Kependudukan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>mobile/kependudukan/Kependudukanmain/get_data/2019" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Kependudukan</span></a>  
                        </li>
                        <!-- Menu Pajak -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Pajak</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>mobile/pajak/bppdapi/index_data_realisasi">Grafik Realisasi dan Target Pajak</a></li>
                                <!-- <li><a href="<?php print_r(base_url());?>index.php/bp2d/Bppdapi/index_pbb">Cek Tagihan PBB</a></li> -->
                           </ul>
                        </li>
                        <!-- Menu Kepegawaian -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>mobile/kepegawaian/kepegawaianmain/show_detail" aria-expanded="false"><i class="fa fa-address-card-o"></i><span class="hide-menu">Kepegawaian</span></a>  
                        </li>
                         <!-- Menu Kewilayahan -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>mobile/kewilayahan/kewilayahanmain/kewilayahan" aria-expanded="false"><i class="mdi mdi-crosshairs-gps"></i><span class="hide-menu">Kewilayahan</span></a>  
                         <!-- Menu Kemasyarakat - Dinas Sosial -->
                        <!-- <li> <a class="has-arrow waves-effect waves-dark" href="<?php print_r(base_url());?>spm/" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu"></span></a>  
                        </li> -->

                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Kemasyarakatan</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>mobile/surat_miskin/suratmiskinmain">Surat Pernyataan Miskin</a></li>
                            </ul>
                        </li>

                        <!-- Menu PDAM -->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-tint"></i><span class="hide-menu">PDAM</span></a>
                         <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>data/pdam/pelanggan">Grafik Data Pelanggan PDAM</a></li>
                                <li><a href="<?php print_r(base_url());?>data/pdam/pengaduan">Grafik Data Pengaduan</a></li>
                                <li><a href="<?php print_r(base_url());?>data/pdam/anjugan">Data Lokasi Air Siap Minum dan Air Belum Siap Minum</a></li>
                                 <li><a href="<?php print_r(base_url());?>data/pdam/cek_tagihan">Cek Tagihan PDAM</a></li>
                            </ul>
                        </li>


                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-arrange-send-backward"></i><span class="hide-menu">Ekonomi</span></a>
                            <ul aria-expanded="false" class="collapse" style="height: 0px;">
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Pendapatan</a>
                                    <ul aria-expanded="false" class="collapse in" style="">
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_object_pad">Objek PAD</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                                    </ul>
                                </li>
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Belanja</a>
                                    <ul aria-expanded="false" class="collapse in" style="">
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                                    </ul>
                                </li>
                                <li> <a class="has-arrow" href="#" aria-expanded="false">Pembiayaan</a>
                                    <ul aria-expanded="false" class="collapse in" style="">
                                        <li><a href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <!-- Menu Kesehatan-->
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-hospital"></i><span class="hide-menu">Kesehatan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>mobile/kesehatan/mainbumil/get_data_by_kelurahan">Data Ibu Hamil</a></li>
                                
                            </ul>
                        </li>

                        

                        <!-- Menu SIPEX Pendapatan -->
                        <!-- <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pendapatan</span></a>
                        <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pendapatan_belanja">Dashboard SIPEX</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_kelompok">Kelompok Pendapatan</a></li>
                                 <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_jenis">Jenis Pendapatan</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_object_pad">Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_rincian_object">Rincian Objek PAD</a></li>
                                <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_realisasi_pendapatan">Realisasi Pendapatan</a></li>
                            </ul>
                        </li> -->
                        <!-- Menu SIPEX Belanja -->
                        <!-- <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Belanja</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_kelompok">Kelompok Belanja</a></li>
                            <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_jenis">Jenis Belanja</a></li>
                            <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_belanja_opd">Belanja Langsung</a></li>
                        </ul>
                        </li> -->
                        <!-- Menu SIPEX Pembiayaan -->
                        <!-- <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">SIPEX | Pembiayaan</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?php print_r(base_url());?>show_report/sipexmain/index_pembiayaan">Pembiayaan</a></li>
                        </ul>
                        </li> -->
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper litle_text">

            <?php
            switch ($page) {
                case 'home_main':
                        include 'mobile/home/home_main.php';
                    break;



                case 'pendidikan_main':
                        include 'mobile/pendidikan/pendidikan_main.php';
                    break;
                
                case 'akreditasi_pd':
                        include 'mobile/pendidikan/akreditasi_detail.php';
                    break;

                case 'akreditasi_rmp':
                        include 'mobile/pendidikan/akreditasi_detail_iso.php';
                    break;

                case 'akreditasi_rma':
                        include 'mobile/pendidikan/akreditasi_detail_ma.php';
                    break;


                case 'pendidikan_sklh':
                        include 'mobile/pendidikan/pendidikan_sklh.php';
                    break;



                case 'penduduk_main':
                        include 'mobile/kependudukan/kependudukan_main.php';
                    break;

                case 'penduduk_rekap':
                        include 'mobile/kependudukan/kependudukan_rekap.php';
                    break;

                case 'penduduk_agama':
                        include 'mobile/kependudukan/kependudukan_agama.php';
                    break;

                case 'penduduk_ktp':
                        include 'mobile/kependudukan/kependudukan_main.php';
                    break;

                case 'penduduk_umur':
                        include 'mobile/kependudukan/kependudukan_umur.php';
                    break;



                case 'pajak_main':
                        include 'mobile/pajak/pajak_main.php';
                    break;


                case 'kepegawaian_main':
                        include 'mobile/kepegawaian/kepegawaian_main.php';
                    break;


                case 'spm_main':
                        include 'mobile/surat_miskin/spm_main.php';
                    break;


                case 'kewilayahan_main':
                        include 'mobile/kewilayahan/kewilayahan_main.php';
                    break;



                case 'sipex_pendapatan_belanja':
                        include 'mobile/sipex/sipex_pendapatan_belanja.php';
                    break;

                case 'sipex_kelompok':
                        include 'mobile/sipex/sipex_kelompok.php';
                    break;

                case 'sipex_jenis':
                        include 'mobile/sipex/sipex_jenis.php';
                    break;

                case 'sipex_object_pad':
                        include 'mobile/sipex/sipex_object_pad.php';
                    break;

                case 'sipex_rincian_object':
                        include 'mobile/sipex/sipex_rincian_object.php';
                    break;

                case 'sipex_realisasi_pendapatan':
                        include 'mobile/sipex/sipex_realisasi_pendapatan.php';
                    break;



                case 'sipex_belanja_kelompok':
                        include 'mobile/sipex/sipex_belanja_kelompok.php';
                    break;

                case 'sipex_belanja_jenis':
                        include 'mobile/sipex/sipex_belanja_jenis.php';
                    break;

                case 'sipex_belanja_opd':
                        include 'mobile/sipex/sipex_belanja_opd.php';
                    break;



                case 'sipex_pembiayaan':
                        include 'mobile/sipex/sipex_pembiayaan.php';
                    break;




                case 'bumil_age':
                        include 'mobile/kesehatan/bumil_age.php';
                    break;

                case 'bumil_hamil':
                        include 'mobile/kesehatan/bumil_main.php';
                    break;

                case 'bumil_tp':
                        include 'mobile/kesehatan/bumil_main.php';
                    break;

                case 'bumil_hapl':
                        include 'mobile/kesehatan/bumil_main.php';
                    break;

                case 'bumil_kelurahan':
                        include 'mobile/kesehatan/bumil_main.php';
                    break;





                case 'pelanggan_main':
                        include 'mobile/pdam/pelanggan_main.php';
                    break;

                case 'pengaduan_main':
                        include 'mobile/pdam/pengaduan_main.php';
                    break;

                case 'tagihan_main':
                        include 'mobile/pdam/cek_tagihan_main.php';
                    break;

                case 'anjungan_main':
                        include 'mobile/pdam/anjungan_main.php';
                    break;
                    

                default:
                        include 'mobile/home/home_main.php';
                    break;
            }
            ?>
            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©2020 Created By Dinas Komunikasi Dan Informatika Kota Malang</footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>adminpress/horizontal/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>adminpress/horizontal/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>adminpress/horizontal/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>adminpress/horizontal/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--morris JavaScript -->
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/raphael/raphael-min.js"></script>
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/morrisjs/morris.min.js"></script>
    <!-- Chart JS -->
    <script src="<?php print_r(base_url());?>adminpress/horizontal/js/dashboard1.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>adminpress/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>