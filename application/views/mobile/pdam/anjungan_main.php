            <!-- maps -->
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
            
            <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Anjungan Air Siap Minum PDAM KOTA MALANG</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="map" style="width: 100%; height: 700px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

    <script>
        var data_main = JSON.parse(<?php print_r($json_data);?>);
        

        var map = L.map('map').setView([ -7.9771206, 112.6340291], 13);
        mapLink = 
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(map);

        // for () {
        
        // }
        marker = new L.marker(["-7.965180177198396", "112.61688396852844"])
                .bindPopup("UNIV.NEG MALANG JL.SURABAYA")
                .addTo(map);

        var LeafIcon = L.Icon.extend({
        options: {
                iconSize:     [40, 40]
            }
        });

        var water_mentah = new LeafIcon({iconUrl: '<?php print_r(base_url());?>assets/images/ic_water_mentah.png'}),
            water_matang = new LeafIcon({iconUrl: '<?php print_r(base_url());?>assets/images/ic_water.png'});

        for (let i in data_main) {
            var marker_anjungan = water_mentah;
            if(data_main[i].sts == "1"){
                marker_anjungan = water_matang;
            }
            // console.log(data_main[i].txt);
            marker = new L.marker(data_main[i].loc)
                .bindPopup(data_main[i].txt)
                .addTo(map);
        }     
    </script>