<?php
    $list_of_month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

    $str_periode = "";

    if(isset($periode)){
        if($periode){
            $periode_th = substr($periode, 0, 4);
            $periode_bln = substr($periode, 4, 2);

            $str_periode = $list_of_month[(int)$periode_bln]." ".$periode_th;
        }
    }
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Rekap Pengaduan Pelanggan PDAM Kota Malang Periode <?php print_r($str_periode);?></h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <div class="row" id="out_body">
                                    
                                </div> -->

                                <div id="chartdiv"  style="width: 100%; height: 500px;" ></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">
    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d"),
          am4core.color("#FFB6C1"),
          am4core.color("#FFE4B5")
        ];
      }
    }

    var data_all = JSON.parse('<?php print_r($all)?>');

    $(document).ready(function(){
        create_chart(data_all);
    });


    function create_chart(data){
        am4core.ready(function() {

        // Themes begin
          am4core.useTheme(am4themes_myTheme);
          // am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdiv", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = data;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "value";
        series.dataFields.category = "name";

        }); // end am4core.ready()
    }   
</script>