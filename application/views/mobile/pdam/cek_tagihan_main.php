            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Cek Tagihan PDAM Kota Malang</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="control-label" style="color:#ffffff">Nomor Saldo</label>
                                            <input type="number" class="form-control" id="nosal" name="nosal" placeholder="Nomor Saldo">
                                            <p style="color: red;" id="nosal_er"></p>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="button" id="btn_batal" class="btn btn-default text-right">Batal</button>&nbsp;&nbsp;
                                            <button type="button" id="cek_pbb" class="btn btn-info text-right">Cek</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                    <h1 class="h3 mb-0 text-gray-800">Detail Tagihan</h1>
                                </div>
                                
                                <div class="card shadow mb-6">
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary"></h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 px-3 mt-4">
                                            <div class="card-block px-3">
                                                <table class="table table-hover" style="color:#000000">
                                                  <tbody> 
                                                    <tr>
                                                      <td><b>Nomor Saldo</b></td>
                                                      <td>: <span id="out_nosal">117xxxx</span></b></td>
                                                    </tr>
                                                    <tr>
                                                      <td><b>Nama Pelanggan</b></td>
                                                      <td>: <span id="out_nama_wp"></span></td>
                                                    </tr>
                                                    <tr>
                                                      <td><b>Alamat</b></td>
                                                      <td>: <span id="out_alamat"></span></td>
                                                    </tr>
                                                    <tr>
                                                      <td><b>Golongan</b></td>
                                                      <td>: <span id="out_golongan"></span></td>
                                                    </tr>
                                                    <tr>
                                                      <td><b>Status Pembayaran</b></td>
                                                      <td>: <span id="out_sts"></span></td>
                                                    </tr>
                                                  </tbody>
                                                </table>      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">
  var month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
  $(document).ready(function(){

  });

  $("#cek_pbb").click(function(){
    get_data();
  });
  $("#nosal").keypress(function(e){
    if (e.which == 13) {
      get_data();
    }
  });

  $("#btn_batal").click(function(){

  });

  function currency(x){
      return x.toLocaleString('us-EG');
  }

  function clear_data(){
    $("#out_nosal").html("");
    $("#out_nama_wp").html("");
    $("#out_alamat").html("");
    $("#out_list_body").html("");
  }

  function get_data(){
    var data_main =  new FormData();
    data_main.append('token', "X00W");
    data_main.append('nosal', $("#nosal").val()); 

    $.ajax({
        url: "<?php echo base_url()."show_report/pdammain/check_tagihan_new";?>", // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            console.log(res);
            show_resposne(res);
        }
    });
  }

  function show_resposne(res){
    var data = JSON.parse(res);
    // console.log(data);
    if(data){
        // var main_data = JSON.parse(data.msg_detail);
        // console.log(main_data);

        if(data.success){
          var pelanggan = data.plg;
          $("#out_nosal").html(pelanggan.nosal);
          $("#out_nama_wp").html(pelanggan.nama);
          $("#out_alamat").html(pelanggan.alamat);
          $("#out_golongan").html(pelanggan.golongan);
          $("#out_sts").html(pelanggan.status);
          var list_tunggakan = pelanggan.tagihan;

          var str_table = "";
          // for (let element_item in list_tunggakan) {
            for (let it in list_tunggakan) {
              str_table += "<tr><td>"+month[parseInt(list_tunggakan[it]["periode"].substr(4, 2))]+" - "+list_tunggakan[it]["periode"].substr(0, 4)+
              "</td><td> Rp. "+currency(list_tunggakan[it]["total"])+"</td></tr>";
            }
          // }

          $("#out_list_body").html(str_table);
        }else{
          $("#nosal_er").html("Nomor Saldo tiak terdaftar");
        }
    }else{
        clear_data();
    }
  }
</script>