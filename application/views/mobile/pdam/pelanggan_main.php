

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Rekap Pelanggan PDAM Kota Malang</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Pilih Mode Chart</label>
                                            <br>
                                            <input type="radio" name="mode" id="mode1" value="0" checked="">
                                            <label for="mode1">Agragat Pelanggan Keseluruhan</label>
                                            <br>

                                            <input type="radio" name="mode" id="mode2" value="1">
                                            <label for="mode2">Agragat Pelanggan Berdasarkan Area</label>
                                            <br>
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-12" id="div_area">
                                        <div class="form-group">
                                            <label>Pilih Area</label>
                                            <select class="form-control custom-select" name="type_filter" id="type_filter">
                                                   
                                                <?php
                                                    if(isset($district)){
                                                        if($district){
                                                            foreach ($district as $key => $value) {
                                                                // print_r($value);
                                                                print_r("<option value=\"".$key."\">AREA ".$key."</option>");
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <div class="row" id="out_body">
                                    
                                </div> -->

                                <div id="chartdiv"  style="width: 100%; height: 800px;" ></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">
    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d"),
          am4core.color("#FFB6C1"),
          am4core.color("#FFE4B5")
        ];
      }
    }

    var detail = JSON.parse('<?php print_r($detail)?>');
    var chart_all = JSON.parse('<?php print_r($chart_all)?>');

    $(document).ready(function(){
        // set_kecamatan();
        // set_kelurahan();

        // create_chart();

        mode_change();
    });

    $("#kec").change(function(){
        set_kelurahan();
        mode_change();
    }); 

    $("#kel").change(function(){
        
    }); 

    $("#type_filter").change(function(){
        mode_change();
    });

    $("#set_filter").click(function(){
        var type_filter = $("#type_filter").val();

        set_next_page(type_filter);
    });

   function set_next_page(param){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var url = "";

        switch(param) {
          case "get_data_by_age":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_kelurahan":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_hamil":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_hapl":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_tp":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
          default:
            
        }

        window.location.href = url;
    }


    function set_kecamatan(){
        console.log(data_wilayah);
        var str_op_val = "";
        for(let elm in data_wilayah){
            if(elm != "1"){
                var nama_kecmatan = data_wilayah[elm].title.kecamatan;
                str_op_val += "<option value=\""+elm+"\">"+nama_kecmatan+"</option>";
            }
        }

        $("#kec").html(str_op_val);
    }

    function set_kelurahan(){
        var val_kec = $("#kec").val();

        var str_op_val = "";
        for(let elm in data_wilayah[val_kec].item){
            if(elm != "1"){
                // console.log(data_wilayah[val_kec].item[elm].idkelurahan);
                var nama_kelurahan = data_wilayah[val_kec].item[elm].kelurahan;
                var id_kelurahan = data_wilayah[val_kec].item[elm].idkelurahan;
                str_op_val += "<option value=\""+id_kelurahan+"\">"+nama_kelurahan+"</option>";
            }
        }

        $("#kel").html(str_op_val);
    }



    $("input[name='mode']").change(function(){
        mode_change();
    });


    function mode_change(){
        var mode = $("input[name='mode']:checked").val();
        var type_filter = $("#type_filter").val();

        if(mode == "1"){
            $("#div_area").removeAttr("hidden", true);
            // console.log(detail);
            create_chart(detail[type_filter]);
        }else{
            $("#div_area").attr("hidden", true);
            create_chart(chart_all);
        }
    }

    function create_chart(data){
        am4core.ready(function() {

        // Themes begin
          am4core.useTheme(am4themes_myTheme);
          // am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdiv", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = data;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "value";
        series.dataFields.category = "name";

        }); // end am4core.ready()
    }   
</script>