<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Surat Pernyataan Miskin</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row" id="out_body">
                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        
        var id_global;

        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};

        function am4themes_myTheme(target) {
          if (target instanceof am4core.ColorSet) {
            target.list = [
              am4core.color("#EC7063"),
              am4core.color("#AF7AC5"),
              am4core.color("#5DADE2"),
              am4core.color("#48C9B0"),
              am4core.color("#F4D03F"),
              am4core.color("#E67E22"),
              am4core.color("#A6ACAF"),
              am4core.color("#1A5276"),
              am4core.color("#142c6d")
            ];
          }
        }

        $(document).ready(function(){
            // console.log(data_label);
            // console.log(data_json_select);
            console.log(data_json);
            
            get_data();
        });

        function get_data(){
            create_canvas();

            for(let i in data_json){
                create_chart("chart_"+i, data_json[i].title, data_json[i].item);    
            }

            // console.log(data_json[filter_jenis][filter_kategori][filter_bln]["PENDUDUK_AWAL_BULAN_INI"][filter_kecamatan]);

            // $("#myLargeModalLabel").html(list_kecamatan[id]);
            // $("#surya").modal('show');
        }


        function create_chart(div_chart, kec, data_main){
            am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            am4core.useTheme(am4themes_myTheme);
            // am4core.useTheme(am4themes_dark);
            // Themes end

            // Create chart instance
            var chart = am4core.create(div_chart, am4charts.XYChart3D);

            // Add data
            chart.data = data_main;

            // Create axes
            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "keterangan";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.renderer.labels.template.hideOversized = false;
            categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.tooltip.label.rotation = 270;
            categoryAxis.tooltip.label.horizontalCenter = "right";
            categoryAxis.tooltip.label.verticalCenter = "middle";

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = kec;
            valueAxis.title.fontWeight = "bold";

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries3D());
            series.dataFields.valueY = "value";
            series.dataFields.categoryX = "keterangan";
            series.name = "value";
            series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            columnTemplate.stroke = am4core.color("#FFFFFF");

            columnTemplate.adapter.add("fill", function(fill, target) {
              return chart.colors.getIndex(target.dataItem.index);
            })

            columnTemplate.adapter.add("stroke", function(stroke, target) {
              return chart.colors.getIndex(target.dataItem.index);
            })

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.strokeOpacity = 0;
            chart.cursor.lineY.strokeOpacity = 0;

            }); // end am4core.ready()

        }


        function create_canvas(){
            var str_option = "";
            for(let i in data_json){
                str_option += "<div class=\"col-md-12\"><div id=\"chart_"+i+"\" style=\"width: 100%; height: 500px;\"></div></div>";
                // console.log(i);
            }

            // console.log(str_option);

            $("#out_body").html(str_option);
        }

        function set_val_th(){
            var th_first = "<?php print_r($th_first);?>";

            $("#th_first").val(th_first);
        }
    </script>