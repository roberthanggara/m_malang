<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Pembiayaan</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK PENDAPATAN</div>
                            <div class="card-body">
                                <div id="chart_penerimaan" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK BELANJA</div>
                            <div class="card-body">
                                <div id="chart_pengeluaran" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script>
    var data = JSON.parse('<?php print_r($list_data);?>');
    console.log(data);

    $(document).ready(function(){
        set_data();
    });

    function currency(x){
        return x.toLocaleString('us-EG');
    }
    
    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d")
        ];
      }
    }

    function set_data(){
        var main_data = data.msg_main;
        var detail_data = data.msg_detail;

            var core_data = detail_data.response.main_item;

            // console.log(core_data);
                var data_penerimaan    = core_data.penerimaan.nominal;
                var data_pengeluaran = core_data.pengeluaran.nominal;

            console.log(data_penerimaan);
            console.log(data_pengeluaran);

        var obj_data_penerimaan = [
            {
                "keterangan":"Pagu",
                "nominal":data_penerimaan.target
            },{
                "keterangan":"Realisasi penerimaan",
                "nominal":data_penerimaan.penerimaan
            }
        ];

        var obj_data_pengeluaran = [
            {
                "keterangan":"Target",
                "nominal":data_pengeluaran.target
            },{
                "keterangan":"Penerimaan",
                "nominal":data_pengeluaran.penerimaan
            }
        ];

        create_chart(obj_data_penerimaan, "chart_penerimaan", "penerimaan Daerah");
        create_chart(obj_data_pengeluaran, "chart_pengeluaran", "pengeluaran Daerah");

        // console.log(obj_data_penerimaan);
    }

    function create_chart(data_chart, id_chart, title){
        am4core.ready(function() {

        // Themes begin
        // am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart3D);

        // Add data
        chart.data = data_chart;

        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "keterangan";
        categoryAxis.renderer.labels.template.rotation = 360;
        categoryAxis.renderer.minGridDistance = 50;
        categoryAxis.renderer.labels.template.horizontalCenter = "center";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        // categoryAxis.tooltip.label.rotation = 270;
        // categoryAxis.tooltip.label.horizontalCenter = "right";
        // categoryAxis.tooltip.label.verticalCenter = "middle";

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = title;
        valueAxis.title.fontWeight = "bold";
        valueAxis.min = 0;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 100;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;

            axisTooltip.dy = 0;

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = "nominal";
        series.dataFields.categoryX = "keterangan";
        series.name = "nominal";
        series.tooltipText = "{categoryX}: Rp. [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", function(fill, target) {
          return chart.colors.getIndex(target.dataItem.index);
        })

        columnTemplate.adapter.add("stroke", function(stroke, target) {
          return chart.colors.getIndex(target.dataItem.index);
        })

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;

        }); // end am4core.ready()
    }

</script>