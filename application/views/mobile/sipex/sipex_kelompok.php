<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Kelompok Pendapatan</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK KELOMPOK PENDAPATAN</div>
                            <div class="card-body">
                                <div id="chart_data" style="width: 100%; height: 600px; "></div>
                            </div>
                        </div>
                    </div>

                    
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script>
    var data = JSON.parse('<?php print_r($list_data);?>');
    // console.log(data);

    $(document).ready(function(){
        set_data();
    });

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          // warna biru
          am4core.color("#00BFFF"),
            
          // warna orange
          am4core.color("#FF8C00"), 

          // warna hijau
          am4core.color("#32CD32")
        ];
      }
    }

    function set_data(){
        var main_data = data.msg_main;
        var detail_data = data.msg_detail;

            var core_data = detail_data.response.main_item;
                var data_kelompok    = core_data.kelompok;
               
            // console.log(data_kelompok);

        create_chart(data_kelompok, "chart_data", "Pendapatan Daerah menurut Kelompok");
        // create_table(data_kelompok);
    }

    function create_table(data){
        var str_table = "";
        for(let i in data){
            // console.log(data[i]);
            str_table += "<tr>"+
                            "<th>"+data[i].name+"</th>"+
                            "<th align=\"right\">RP. "+currency(data[i].pagu)+"</th>"+
                            "<th align=\"right\">RP. "+currency(data[i].nilai)+"</th>"+
                        "</tr>";
        }

        $("#out_table").html(str_table);
    }

    function create_chart(data_chart, id_chart, title){
        am4core.ready(function() {

        // Themes begin
        // am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart);

        // Add data
        chart.data = data_chart;

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "name";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 75;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 75;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "name";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "Rp. {valueX}[/]";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            // categoryLabel.label.text = "{name}";
            categoryLabel.label.text = name;
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
            // console.log(categoryLabel);
        }

        createSeries("pagu", "Pagu Pendapatan");
        createSeries("nilai", "Ralisasi");

        }); // end am4core.ready()
    }
</script>