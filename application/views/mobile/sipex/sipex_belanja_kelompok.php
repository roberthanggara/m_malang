<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Kelompok Belanja</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK BELANJA LANGSUNG</div>
                            <div class="card-body">
                                <div id="chart_belanja_langsung" style="width: 100%; height: 600;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK BELANJA TIDAK LANGSUNG</div>
                            <div class="card-body">
                                <div id="chart_belanja_tak_langsung" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script>
    var data = JSON.parse('<?php print_r($list_data);?>');
    console.log(data);

    $(document).ready(function(){
        set_data();
    });

    function currency(x){
        return x.toLocaleString('us-EG');
    }
    
    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
         am4core.color("#FF7F50"),
           // HIJAU
          am4core.color("#32CD32"),
        ];
      }
    }

    function set_data(){
        var main_data = data.msg_main;
        var detail_data = data.msg_detail;

            var core_data = detail_data.response.main_item;
                var data_belanja_langsung    = core_data.belanja_langsung.nominal;
                var data_belanja_tak_langsung = core_data.belanja_tak_langsung.nominal;

            console.log(data_belanja_langsung);
            console.log(data_belanja_tak_langsung);

        var obj_data_belanja_langsung = [
            {
                "keterangan":"Pagu",
                "nominal":data_belanja_langsung.pagu
            },{
                "keterangan":"Realisasi Belanja Langsung",
                "nominal":data_belanja_langsung.realisasi
            }
        ];

        var obj_data_belanja_tak_langsung = [
            {
                "keterangan":"Pagu",
                "nominal":data_belanja_tak_langsung.pagu
            },{
                "keterangan":"Realisasi Belanja Tidak Langsung",
                "nominal":data_belanja_tak_langsung.realisasi
            }
        ];

        create_chart(obj_data_belanja_langsung, "chart_belanja_langsung", "Belanja Langsung Daerah");
        create_chart(obj_data_belanja_tak_langsung, "chart_belanja_tak_langsung", "Belanja Tidak Langsung Daerah");

        // console.log(obj_data_belanja);
    }

    function create_chart(data_chart, id_chart, title){
        am4core.ready(function() {

        // Themes begin
        // am4core.useTheme(am4themes_dark);
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart3D);

        // Add data
        chart.data = data_chart;

        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "keterangan";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.renderer.labels.template.hideOversized = false;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.tooltip.label.rotation = 270;
        categoryAxis.tooltip.label.horizontalCenter = "right";
        categoryAxis.tooltip.label.verticalCenter = "middle";

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = title;
        valueAxis.title.fontWeight = "bold";
        valueAxis.min = 0;

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = "nominal";
        series.dataFields.categoryX = "keterangan";
        series.name = "nominal";
        series.tooltipText = "{categoryX}: Rp. [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", function(fill, target) {
          return chart.colors.getIndex(target.dataItem.index);
        })

        columnTemplate.adapter.add("stroke", function(stroke, target) {
          return chart.colors.getIndex(target.dataItem.index);
        })

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;

        }); // end am4core.ready()
    }

</script>