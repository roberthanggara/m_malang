<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Objek PAD</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK PAJAK DAERAH</div>
                            <div class="card-body">
                                <div id="chart_data_pajak_daerah" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK DATA RETRIBUSI DAERAH</div>
                            <div class="card-body">
                                <div id="chart_data_retribusi_daerah" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK PENGELOLAAN KEKAYAAN YANG DI PISAHKAN</div>
                            <div class="card-body">
                                <div id="chart_data_kekayaan" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">GRAFIK LAIN - LAIN PAD</div>
                            <div class="card-body">
                                <div id="chart_data_lain_lain" style="width: 100%; height: 600px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script>
    var data = JSON.parse('<?php print_r($list_data);?>');
    // console.log(data);

    $(document).ready(function(){
        set_data();
    });

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d")
        ];
      }
    }

    function set_data(){
        var main_data = data.msg_main;
        var detail_data = data.msg_detail;

            var core_data = detail_data.response.main_item;
                var data_pajak_daerah    = core_data.pajak_daerah.list;
                var title_pajak_daerah   = core_data.pajak_daerah.title;
                var total_pajak_daerah   = core_data.pajak_daerah.total;

            create_chart(data_pajak_daerah, "chart_data_pajak_daerah", title_pajak_daerah);
            // create_table(data_pajak_daerah, "out_table_pajak_daerah");

                var data_retribusi_daerah    = core_data.retribusi_daerah.list;
                var title_retribusi_daerah   = core_data.retribusi_daerah.title;
                var total_retribusi_daerah   = core_data.retribusi_daerah.total;

            create_chart(data_retribusi_daerah, "chart_data_retribusi_daerah", title_retribusi_daerah);
            // create_table(data_retribusi_daerah, "out_table_retribusi_daerah");

                var data_kekayaan    = core_data.kekayaan.list;
                var title_kekayaan   = core_data.kekayaan.title;
                var total_kekayaan   = core_data.kekayaan.total;

            create_chart(data_kekayaan, "chart_data_kekayaan", title_kekayaan);
            // create_table(data_kekayaan, "out_table_kekayaan");

                var data_lain_lain    = core_data.lain_lain.list;
                var title_lain_lain   = core_data.lain_lain.title;
                var total_lain_lain   = core_data.lain_lain.total;

            create_chart(data_lain_lain, "chart_data_lain_lain", title_lain_lain);
            // create_table(data_lain_lain, "out_table_lain_lain");
    }

    function create_table(data, id_table){
        var str_table = "";
        for(let i in data){
            // console.log(data[i]);
            str_table += "<tr>"+
                            "<td>"+data[i].nama_rekening+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].pagu))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].nominal))+"</td>"+
                        "</tr>";
        }

        $("#"+id_table).html(str_table);
    }

    function create_chart(data_chart, id_chart, title){
        // console.log(data_chart);
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // am4core.useTheme(am4themes_dark);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart);

        // Add data
        chart.data = data_chart;

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama_rekening";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 75;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.fill = am4core.color("#07BEB8");
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 75;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "nama_rekening";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "Rp. {valueX}[/]";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            // categoryLabel.label.text = "{name}";
            categoryLabel.label.text = name;
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
            // console.log(categoryLabel);
        }

        createSeries("pagu", "Pagu Pendapatan");
        createSeries("nominal", "Ralisasi");

        }); // end am4core.ready()
    }

</script>