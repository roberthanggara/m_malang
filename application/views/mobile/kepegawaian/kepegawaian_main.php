<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Kepegawaian</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>UPD dan Badan</label>
                                            <select class="form-control custom-select" id="filter_jenis" name="filter_jenis">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select class="form-control custom-select" id="filter_kategori" name="filter_kategori">
                                                <option value="golongan">Golongan</option>
                                                <option value="jenis_kelamin">Jenis Kelamin</option>
                                                <option value="agama">Agama</option>
                                                <option value="status">Status Kawin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="out_body">
                    
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script>
    var data_main = JSON.parse('<?php print_r($data_json);?>');

    $(document).ready(function(){
        create_jenis_list();

        set_data();
    });

    $("#filter_kategori").change(function(){
        set_data();
    });

    $("#filter_jenis").change(function(){
        set_data();
    });

    function create_jenis_list(){
        var str_list_jenis = "";
        for(let item in data_main){
            str_list_jenis += "<option value=\""+item+"\">"+data_main[item].unit_kerja+"</option>";
        }

        $("#filter_jenis").html(str_list_jenis);
    }

    function set_data(){
        var jenis = $("#filter_jenis").val();
        var kategori = $("#filter_kategori").val();

        switch(kategori) {
        case "golongan":
            create_canvas_golongan();

            set_chart_golongan(jenis);
            break;
        
        case "jenis_kelamin":
            create_canvas_jenis_kelamin();
            
            set_chart_jenis_kelamin(jenis);
            break;
        
        case "agama":
            create_canvas_agama();
            
            set_chart_agama(jenis);
            break;
        
        case "status":
            create_canvas_status();

            set_chart_status(jenis);
            break;
        
        default:
            // code block
        }
    }

    function create_canvas_golongan(){
        var str_option = "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Golongan Keseluruhan</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_golongan_t\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+

                        "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Golongan I</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_golongan_1\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+

                        "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Golongan II</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_golongan_2\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+

                        "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Golongan III</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_golongan_3\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+

                        "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Golongan IV</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_golongan_4\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
            

        $("#out_body").html(str_option);
    }

    function create_canvas_jenis_kelamin(){
        var str_option = "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Berdasarkan Jenis Kelamin</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_jenis_kelamin\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
            

        $("#out_body").html(str_option);
    }

    function create_canvas_agama(){
        var str_option = "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Berdasarkan Agama</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_agama\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";

        $("#out_body").html(str_option);
    }

    function create_canvas_status(){
        var str_option = "<div class=\"col-md-12\">"+
                            "<div class=\"card\">"+
                                "<div class=\"card-header\"><h6>Berdasarkan Status Perkawinan</h6></div>"+
                                "<div class=\"card-body\">"+
                                    "<div class=\"col-md-12\"><div id=\"chart_status\" style=\"width: 100%; height: 400px;\"></div></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
        

        $("#out_body").html(str_option);
    }


    function set_chart_golongan(i){
        set_graph("chart_golongan_t", data_main[i].sum_golongan);

        set_graph("chart_golongan_1", data_main[i].jumlah_golongan_I);
        set_graph("chart_golongan_2", data_main[i].jumlah_golongan_II);
        set_graph("chart_golongan_3", data_main[i].jumlah_golongan_III);
        set_graph("chart_golongan_4", data_main[i].jumlah_golongan_IV);
    }

    function set_chart_jenis_kelamin(i){
        set_graph("chart_jenis_kelamin", data_main[i].gender);
    }

    function set_chart_agama(i){
        set_graph("chart_agama", data_main[i].agama);
    }

    function set_chart_status(i){
        set_graph("chart_status", data_main[i].status_kawin);
    }


    function set_graph(id_chart_div, data_main){
        am4core.ready(function() {

        am4core.useTheme(am4themes_animated);
        // am4core.useTheme(am4themes_dark);

        var chart = am4core.create(id_chart_div, am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 5; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = data_main;

        var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "value";
            series.dataFields.category = "keterangan";
        });

    }
</script>