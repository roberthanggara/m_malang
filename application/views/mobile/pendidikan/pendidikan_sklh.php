            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
            
            <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">DATA SEBARAN SEKOLAH DI KOTA MALANG</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Data</label>
                                            <select class="form-control custom-selec" id="jenis_sklh" name="jenis_sklh">
                                                <option value="paud">Pendidikan Pra Sekolah</option>
                                                <option value="dasar">Pendidikan Dasar</option>
                                                <option value="menengah">Pendidikan Menengah Atas</option>
                                                <option value="non_formal">Pendidikan Non Formal</option>
                                                <option value="khusus">Pendidikan Khusus</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <select class="form-control custom-selec" id="select_kec" name="select_kec">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Kelurahan</label>
                                            <select class="form-control custom-selec" id="select_kel" name="select_kel">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-danger" id="remove_marker" name="remove_marker">Reset</button>

                                        <button class="btn btn-info" id="all_data_marker" name="all_data_marker">Lihat Semua</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <div class="row">
                    <div id="map" style="width: 100%; height: 600px"></div>
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        var all_marker = [];

        var map = L.map('map').setView([ -7.9771206, 112.6340291], 13);
        mapLink = 
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(map);

        var LeafIcon = L.Icon.extend({
        options: {
                iconSize:     [40, 40]
            }
        });

        var water_mentah = new LeafIcon({iconUrl: '<?php print_r(base_url());?>assets/images/ic_water_mentah.png'}),
            water_matang = new LeafIcon({iconUrl: '<?php print_r(base_url());?>assets/images/ic_water.png'});

        $(document).ready(function(){
            set_select_kec();
            set_select_kel();

            get_all();
        });

        $("#jenis_sklh").change(function(){
            var jenis_sklh = $("#jenis_sklh").val();
            var form_data = new FormData();
            form_data.append('id', "0");
            $.ajax({
                url: "<?php print_r(base_url());?>pendidikan_new/pendidikan_sklh/get_data_x/"+jenis_sklh, // point to server-side PHP script 
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                async: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(res){
                    //alert("surya");
                    // console.log(list_url_chart[id]);
                      
                    // dumb_chart = res;

                    // console.log(res);
                    jenis_sklh_res(res);
                      
                }
            });
        });

        $("#select_kec").change(function(){
            set_select_kel();
            get_kec_kel();
        });

        $("#select_kel").change(function(){
            get_kec_kel();
        });

        $("#remove_marker").click(function(){
            clear_marker();
        });

        $("#all_data_marker").click(function(){
            get_all();
        });


        function set_select_kec() {
            var str_kec = "";
            for (let ik in data_main) {
                str_kec += "<option value=\""+ik+"\">"+ik+"</option>";
            }
            $("#select_kec").html(str_kec);
        }

        function set_select_kel() {
            var kec = $("#select_kec").val();

            var str_kel = "";
            for (let il in data_main[kec]) {
                str_kel += "<option value=\""+il+"\">"+il+"</option>";
            }
            $("#select_kel").html(str_kel);
        }

        function get_all(){
            clear_marker();
            all_marker = [];

            for (let ik in data_main) {
                for (let il in data_main[ik]) {
                    for (let im in data_main[ik][il]) {
                        var nama_sekolah    = data_main[ik][il][im].nama_sekolah;
                        var alamat_jalan    = data_main[ik][il][im].alamat_jalan;
                        var nomor_telepon   = data_main[ik][il][im].nomor_telepon;
                        var email           = data_main[ik][il][im].email;
                        var website         = data_main[ik][il][im].website;

                        if(alamat_jalan == null){
                            alamat_jalan = "-";
                        }

                        if(nomor_telepon == null){
                            nomor_telepon = "-";
                        }

                        if(email == null){
                            email = "-";
                        }

                        if(website == null){
                            website = "-";
                        }
                        
                        marker = new L.marker(data_main[ik][il][im].latlng)
                                    .bindPopup("<div><b>"+nama_sekolah+"</b></div>"+
                                                "<div><label>Alamat Jalan: </label>"+alamat_jalan+"</div>"+
                                                "<div><label>Nomor Telephon: </label>"+nomor_telepon+"</div>"+
                                                "<div><label>Email: </label>"+email+"</div>"+
                                                "<div><label>Website: </label>"+website+"</div>")
                                    .addTo(map);

                        all_marker.push(marker);
                    }
                }               
            }
        }

        function get_kec_kel(){
            clear_marker();
            all_marker = [];

            var ik = $("#select_kec").val();
            var il = $("#select_kel").val();

            
                    for (let im in data_main[ik][il]) {

                        var nama_sekolah    = data_main[ik][il][im].nama_sekolah;
                        var alamat_jalan    = data_main[ik][il][im].alamat_jalan;
                        var nomor_telepon   = data_main[ik][il][im].nomor_telepon;
                        var email           = data_main[ik][il][im].email;
                        var website         = data_main[ik][il][im].website;

                        if(alamat_jalan == null){
                            alamat_jalan = "-";
                        }

                        if(nomor_telepon == null){
                            nomor_telepon = "-";
                        }

                        if(email == null){
                            email = "-";
                        }

                        if(website == null){
                            website = "-";
                        }
                        
                        marker = new L.marker(data_main[ik][il][im].latlng)
                                    .bindPopup("<div><b>"+nama_sekolah+"</b></div>"+
                                                "<div><label>Alamat Jalan: </label>"+alamat_jalan+"</div>"+
                                                "<div><label>Nomor Telephon: </label>"+nomor_telepon+"</div>"+
                                                "<div><label>Email: </label>"+email+"</div>"+
                                                "<div><label>Website: </label>"+website+"</div>")
                                    .addTo(map);
                        all_marker.push(marker);
                    }
        }

        function clear_marker(){
            for (let im in all_marker) {
                map.removeLayer(all_marker[im]);
            }
        }

        function jenis_sklh_res(res){
            var data_json_res = JSON.parse(res);
            console.log(data_json_res);

            data_main = data_json_res;

        }

        $( document ).ajaxComplete(function( event, xhr, settings ) {
          // if ( settings.url === "ajax/test.html" ) {
          //   $( ".log" ).text( "Triggered ajaxComplete handler. The result is " +
          //     xhr.responseText );
          // }
            clear_marker();
            console.log(data_main);
            set_select_kec();
            set_select_kel();

            get_all();
          // console.log(event);
          // console.log(xhr);
          // console.log(settings);
        });
               
    </script>