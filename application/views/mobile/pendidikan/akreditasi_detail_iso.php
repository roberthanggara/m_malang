            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor" id="header_page_1">Data Pendidikan</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row" id="main_data_x">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG
                            </div>
                            <div class="card-body">
                                <div id="chartdiv_akred" style="width: 100%; height: 500px;"></div>                            
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                GRAFIK KURIKULUM/SERTIFIKASI
                            </div>
                            <div class="card-body">
                                <div id="chartdiv_iso" style="width: 100%; height: 500px;"></div>                            
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG BERDASARKAN ZONA
                            </div>
                            <div class="card-body">
                                <div id="chartdiv_akred_z" style="width: 100%; height: 500px;"></div>                            
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                GRAFIK KURIKULUM/SERTIFIKASI BERDASARKAN ZONA
                            </div>
                            <div class="card-body">
                                <div id="chartdiv_iso_z" style="width: 100%; height: 500px;"></div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script>
    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#48C9B0")
        ];
      }
    }

    var data_main = JSON.parse('<?php print_r($data_json);?>');
    $(document).ready(function(){
        set_all_data();
    });

    function set_request(){
        var data_main =  new FormData();
        data_main.append('stsek', $("#stsek").val());
        data_main.append('tahundata', $("#tahundata").val());
        data_main.append('jenjang', $("#jenjang").val());
                                            
        $.ajax({
          url: "<?php echo base_url()."pendidikan_new/pendidikan/index_ma_req";?>",
          dataType: 'html',  // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          data: data_main,                         
          type: 'post',
          success: function(res){
              // console.log(res);
              change_data(res);
              // set_val_update_strata(res, param);
          }
        });
    }

    function change_data(res){
        data_main_new = JSON.parse(res);
        data_main = data_main_new.msg_detail.item;
        console.log(data_main_new.msg_detail.item);
    }

    function set_all_data(){
          $("#header_page_1").html("Grafik "+data_main.title_header+" \n "+data_main.title_main);

          var title_header = data_main.title;

          var data_sum_kec = [];
          var data_all = [];

          var array_sum_item = [];
          var array_sum_sklh = [];

          var array_table = [];
          var no_x = 0; 
          for (let element_item in data_main.item) {
            var no_element_sub = 0;
            
            array_sum_item[element_item] = [];

            var sum_sekolah = 0;

            var tmp_all = [];
            for (let element_sub in data_main.item[element_item]["item"]) {
              var body_table = "";

              // array_sum_item[element_item][element_sub] = [];
              
              if(no_x == 0){
                body_table += "<td align='center'>"+(no_element_sub+1)+"</td><td>"+data_main.item[element_item]["item"][element_sub].nama_kecamatan+"</td><td align='center'>"+data_main.item[element_item]["item"][element_sub].jml_sek+"</td>";

                sum_sekolah += data_main.item[element_item]["item"][element_sub].jml_sek;

                var tmp_sum_kec = {
                  "nama_kecamatan":data_main.item[element_item]["item"][element_sub].nama_kecamatan,
                  "val":data_main.item[element_item]["item"][element_sub].jml_sek
                };

                data_sum_kec.push(tmp_sum_kec);
              }

              tmp_all[no_element_sub] = {};
              // tmp_all[no_x][no_element_sub] = {};
              
              tmp_all[no_element_sub]["nama_kecamatan"] = data_main.item[element_item]["item"][element_sub].nama_kecamatan;


              var no_element_sub_x = 0;
              for (let element_sub_x in data_main.item[element_item]["item"][element_sub]) {
                // console.log(data_main.item[element_item]["item"][element_sub][element_sub_x]);
                if(no_element_sub_x > 1){
                  body_table += "<td align='center'>"+data_main.item[element_item]["item"][element_sub][element_sub_x]+"</td>";

                  if((element_sub_x in array_sum_item[element_item])){
                    array_sum_item[element_item][element_sub_x] += data_main.item[element_item]["item"][element_sub][element_sub_x];
                  }else{
                    array_sum_item[element_item][element_sub_x] = data_main.item[element_item]["item"][element_sub][element_sub_x];
                  }

                  tmp_all[no_element_sub][element_sub_x] = data_main.item[element_item]["item"][element_sub][element_sub_x];
                  // console.log(tmp_all[no_element_sub][element_sub_x]);
                }

                no_element_sub_x++;
              }

              // console.log(tmp_all);

              data_all[no_x] = [];
              data_all[no_x].push(tmp_all);

              if((no_element_sub in array_table)){
                array_table[no_element_sub] += body_table;
              }else{
                array_table[no_element_sub] = body_table;
              }
              
              no_element_sub++;
            }

            if(no_x == 0){
              array_sum_sklh[no_x] = sum_sekolah;    
            }

            no_x++;
          }

          // console.log(array_sum_item);
          var item_graph_sum_all = {};

          var fix_table = "";
          for (let e in array_table) {
            fix_table += "<tr>"+array_table[e]+"</tr>";
          }

          fix_table += "<tr><td colspan='2' align='center'><b>Jumlah Keseluruhan</b></td><td align='center'><b>"+array_sum_sklh[0]+"</b></td>";

          var item_graph_all = {};

          for (let z in array_sum_item) {

            item_graph_all[z] = [];

            // tmp_item_graph_all = 
            for (let n in array_sum_item[z]) {
              var tmp_item_graph_all = {};
              fix_table += "<td align='center'><b>"+array_sum_item[z][n]+"</b></td>";

              if(z == "akred"){
                tmp_item_graph_all["category"] = "AKREDITASI "+n.split("_")[1].toUpperCase();
                tmp_item_graph_all["val"] = array_sum_item[z][n];
              }else if(z == "iso"){
                tmp_item_graph_all["category"] = "SERTIFIKASI "+n.split("_")[1].toUpperCase();
                tmp_item_graph_all["val"] = array_sum_item[z][n];
              }

              // console.log(tmp_item_graph_all);

              item_graph_all[z].push(tmp_item_graph_all);
            }            
          }

          fix_table += "</tr>";

          // console.log(data_all[0][0]);
          // console.log(array_sum_item);

          console.log(data_all);
          set_graph_acred_all(item_graph_all.akred);
          set_graph_iso_all(item_graph_all.iso);

          set_graph_acred(data_all[0][0]);
          set_graph_iso(data_all[1][0]);
    }

    function set_graph_acred_all(data_main){
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_myTheme);
        am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdiv_akred", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = data_main;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "val";
        series.dataFields.category = "category";

        });
    }

    function set_graph_iso_all(data_main){
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_myTheme);
        am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdiv_iso", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = data_main;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "val";
        series.dataFields.category = "category";

        });
    }


    function set_graph_acred(data_main){
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_myTheme);
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv_akred_z", am4charts.XYChart);


        // Add data
        chart.data = data_main;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama_kecamatan";
        categoryAxis.renderer.grid.template.location = 0;


        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.inside = true;
        valueAxis.renderer.labels.template.disabled = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "nama_kecamatan";
            series.sequencedInterpolation = true;

            // Make it stacked
            series.stacked = true;

            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;

            return series;
        }

        createSeries("akred_a", "AKREDITASI A");
        createSeries("akred_b", "AKREDITASI B");
        createSeries("akred_c", "AKREDITASI C");
        createSeries("akred_tt", "AKREDITASI TT");

        // Legend
        chart.legend = new am4charts.Legend();

        });
    }

    function set_graph_iso(data_main){
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_myTheme);
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv_iso_z", am4charts.XYChart);


        // Add data
        chart.data = data_main;

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama_kecamatan";
        categoryAxis.renderer.grid.template.location = 0;


        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.inside = true;
        valueAxis.renderer.labels.template.disabled = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {

        // Set up series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.name = name;
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "nama_kecamatan";
        series.sequencedInterpolation = true;

        // Make it stacked
        series.stacked = true;

        // Configure columns
        series.columns.template.width = am4core.percent(60);
        series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

        // Add label
        var labelBullet = series.bullets.push(new am4charts.LabelBullet());
        labelBullet.label.text = "{valueY}";
        labelBullet.locationY = 0.5;

        return series;
        }

        createSeries("iso_1", "SERTIFIKASI ISO 9001:2000");
        createSeries("iso_2", "SERTIFIKASI 9001:2008");
        createSeries("iso_3", "SERTIFIKASI Proses Sertifikasi");
        createSeries("iso_4", "SERTIFIKASI Belum Bersertifikat");

        // Legend
        chart.legend = new am4charts.Legend();

        });
    }
</script>