            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Pendidikan</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <?php print_r($option);?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Data</label>
                                            <select class="form-control custom-selec" id="jenis_data" name="jenis_data">
                                                <option value="data_master">Data Master</option>
                                                <option value="data_pendidik">Data Pendidik</option>
                                                <option value="data_peserta_didik">Data Peserta Didik</option>
                                            </select> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <div class="row" id="main_data_x">
                    
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script>
    var data                    = JSON.parse('<?php print_r($data_json);?>');
    var data_json_pendidik      = JSON.parse('<?php print_r($data_json_pendidik);?>');
    var data_json_peserta_didik = JSON.parse('<?php print_r($data_json_peserta_didik);?>');
    console.log(data_json_pendidik);

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d")
        ];
      }
    }

    $(document).ready(function(){
        // set_data();
        create_cawan_master();
        get_data();
    });


    $("#jenis_data").change(function(){
        get_data();
    });

    $("#kecamatan").change(function(){
        get_data();
    });
    
    function get_data(){
        var jenis_data = $("#jenis_data").val();
        var id = $("#kecamatan").val();

        console.log(id);
        
        if(jenis_data == "data_master"){
            create_cawan_master();
            set_data(id);
        }else if(jenis_data == "data_pendidik"){
            create_cawan_pendidik();
            set_data_pendidik(id);
        }else if(jenis_data == "data_peserta_didik"){
            create_cawan_pendidik();
            set_data_peserta_didik(id);
        }
        
    }

    function get_data_pendidik(id){
        create_cawan_pendidik();
        // console.log(id);
        // $("#surya").modal('show');
    }


    function create_cawan_pendidik(){
        $("#main_data_x").html("");
        var str_cawan = 
                        // "<div class=\"row\">"+
                            "<div class=\"col-lg-12\">"+
                                "<div class=\"card\">"+
                                    "<div class=\"card-header\" id=\"title_all\">"+

                                    "</div>"+
                                    "<div class=\"card-body\">"+
                                        "<div>"+
                                            "<div id=\"chart_all\" style=\"width: 100%; height: 600px;\"></div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"
                        // "</div>"
                        ;

        $("#main_data_x").html(str_cawan);
    }

   
    function create_cawan_master(){
        $("#main_data_x").html("");

        var str_cawan = "";

        for(let key in data){
            console.log(key);

            str_cawan += 
                        // "<div class=\"row\">"+
                            "<div class=\"col-md-12\">"+
                                "<div class=\"card\">"+
                                    "<div class=\"card-header\" id=\"title_"+key+"\">"+

                                    "</div>"+
                                    "<div class=\"card-body\">"+
                                        // "<h4 class=\"card-title\" id=\"title_"+key+"\"></h4>"+
                                        "<div>"+
                                            "<div id=\"chart_"+key+"\" style=\"width: 100%; height: 600px;\"></div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"
                        // "</div>"
                        ;
        }

        $("#main_data_x").html(str_cawan);
    }




    function set_data(kec){
        // var kec = $("#select_kec").val();
        console.log(data["pd_pra"]["item"][kec]);

        var data_pd_pra = data["pd_pra"]["item"][kec].item;
            var keterangan_pd_pra = data["pd_pra"]["keterangan"];

        var title_kec = data["pd_pra"]["item"][kec].nama;
        $("#myLargeModalLabel").html(title_kec);

        var data_dasar = data["dasar"]["item"][kec].item;
            var keterangan_dasar = data["dasar"]["keterangan"];

        var data_menengah_pertama = data["menengah_pertama"]["item"][kec].item;
            var keterangan_menengah_pertama = data["menengah_pertama"]["keterangan"];

        var data_menengah_atas = data["menengah_atas"]["item"][kec].item;
            var keterangan_menengah_atas = data["menengah_atas"]["keterangan"];

        var data_khusus = data["khusus"]["item"][kec].item;
            var keterangan_khusus = data["khusus"]["keterangan"];

        var data_non_formal = data["non_formal"]["item"][kec].item;
            var keterangan_non_formal = data["non_formal"]["keterangan"];
        

        create_chart(data_pd_pra, "chart_pd_pra", keterangan_pd_pra);
        create_chart(data_dasar, "chart_dasar", keterangan_dasar);
        create_chart(data_menengah_pertama, "chart_menengah_pertama", keterangan_menengah_pertama);
        create_chart(data_menengah_atas, "chart_menengah_atas", keterangan_menengah_atas);
        create_chart(data_khusus, "chart_khusus", keterangan_khusus);
        create_chart(data_non_formal, "chart_non_formal", keterangan_non_formal);


        $("#title_pd_pra").html(keterangan_pd_pra);
        $("#title_dasar").html(keterangan_dasar);
        $("#title_menengah_pertama").html(keterangan_menengah_pertama);
        $("#title_menengah_atas").html(keterangan_menengah_atas);
        $("#title_khusus").html(keterangan_khusus);
        $("#title_non_formal").html(keterangan_non_formal);

        // create_table(data_pd_pra, "table_pd_pra", "title_pd_pra", keterangan_pd_pra);
        // create_table(data_dasar, "table_dasar", "title_dasar", keterangan_dasar);
        // create_table(data_menengah_pertama, "table_menengah_pertama", "title_menengah_pertama", keterangan_menengah_pertama);
        // create_table(data_menengah_atas, "table_menengah_atas", "title_menengah_atas", keterangan_menengah_atas);
        // create_table(data_khusus, "table_khusus", "title_khusus", keterangan_khusus);
        // create_table(data_non_formal, "table_non_formal", "title_non_formal", keterangan_non_formal);
    }

    function set_data_pendidik(kec){
        // var kec = $("#select_kec").val();

        var data_all = data_json_pendidik["all"]["item"][kec].item;
            var nama_kec = data_json_pendidik["all"]["item"][kec].nama;
            var keterangan_all = data_json_pendidik["all"]["keterangan"];

        var title_kec = data_json_pendidik["all"]["item"][kec].nama;
        // $("#myLargeModalLabel").html(title_kec);


        create_chart_pendidik(data_all, "chart_all", keterangan_all);
        $("#title_all").html(keterangan_all);

        // create_table_pendidik(data_all, "table_all", "title_all", nama_kec, keterangan_all);
    }

    function set_data_peserta_didik(kec){
        // var kec = $("#select_kec").val();

        var data_all = data_json_peserta_didik["all"]["item"][kec].item;
            var nama_kec = data_json_peserta_didik["all"]["item"][kec].nama;
            var keterangan_all = data_json_peserta_didik["all"]["keterangan"];

        var title_kec = data_json_peserta_didik["all"]["item"][kec].nama;
        // $("#myLargeModalLabel").html(title_kec);


        create_chart_pendidik(data_all, "chart_all", keterangan_all);
        $("#title_all").html(keterangan_all);

        // create_table_pendidik(data_all, "table_all", "title_all", nama_kec, keterangan_all);
    }




    function create_table(data, id_table, id_header, keterangan){

        console.log(data);
        var str_table = "<thead>"+
                            "<tr>"+
                                "<th>Keterangan</th>"+
                                "<th>Negeri</th>"+
                                "<th>Swasta</th>"+
                                "<th>Total</th>"+
                            "</tr>"+
                        "</thead>"+
                        "<tbody>";

        var tn = 0;
        var ts = 0;
        var ta = 0;

        for(let i in data){
            str_table += "<tr>"+
                            "<td>"+data[i].nama+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].n))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].s))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].a))+"</td>"+
                        "</tr>";

            tn += data[i].n;
            ts += data[i].s;
            ta += data[i].a;
        }
            str_table += "<tr>"+
                            "<td>Total</td>"+
                            "<td align=\"right\">"+tn+"</td>"+
                            "<td align=\"right\">"+ts+"</td>"+
                            "<td align=\"right\">"+ta+"</td>"+
                        "</tr>";

        str_table +=    "</tbody>";

        $("#"+id_table).html(str_table);
        $("#"+id_header).html(keterangan);
    }

    function create_table_pendidik(data, id_table, id_header, keterangan){

        console.log(data);
        var str_table = "<tbody>";

        var tn = 0;
        var ts = 0;
        var ta = 0;
        str_table += "<tr>"+
                        "<th>Keterangan</th>"+
                        "<th>Negeri</th>"+
                        "<th>Swasta</th>"+
                        "<th>Total</th>"+
                    "</tr>";
        for(let i in data){

            str_table += "<tr>"+
                            "<td>"+data[i].nama+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].n))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].s))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(data[i].a))+"</td>"+
                        "</tr>";

            tn += data[i].n;
            ts += data[i].s;
            ta += data[i].a;
        }
            str_table += "<tr>"+
                            "<td>Total</td>"+
                            "<td align=\"right\">"+currency(parseFloat(tn))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(ts))+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(ta))+"</td>"+
                        "</tr>";

        str_table +=    "</tbody>";

        $("#"+id_table).html(str_table);
        $("#"+id_header).html(keterangan);
    }



    function create_chart(data_chart, id_chart, title){
        // console.log(data_chart);
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // am4core.useTheme(am4themes_dark);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart);

        // Add data
        chart.data = data_chart;

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 100;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 200;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "nama";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "{valueX}[/]";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            // categoryLabel.label.text = "{name}";
            categoryLabel.label.text = name;
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
            // console.log(categoryLabel);
        }

        createSeries("n", "Negeri");
        createSeries("s", "Swasta");

        }); // end am4core.ready()
    }

    function create_chart_pendidik(data_chart, id_chart, title){
        // console.log(data_chart);
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart);

        // Add data
        chart.data = data_chart;

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama";
        categoryAxis.numberFormatter.numberFormat = "#";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 100;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 200;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter.numberFormat = "#,###";
        valueAxis.renderer.opposite = true;
        valueAxis.min = 0;

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "nama";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "{valueX}[/]";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            // categoryLabel.label.text = "{name}";
            categoryLabel.label.text = name;
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
            // console.log(categoryLabel);
        }

        createSeries("n", "Negeri");
        createSeries("s", "Swasta");

        }); // end am4core.ready()
    }

    function create_chart1(data_chart, id_chart, title){
        am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_myTheme);
        // Themes end

        // Create chart instance
        var chart = am4core.create(id_chart, am4charts.XYChart);

        // Add data
        chart.data = data_chart;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "nama";
        categoryAxis.renderer.grid.template.opacity = 0;
        categoryAxis.numberFormatter.numberFormat = "#";

        let label = categoryAxis.renderer.labels.template;
            label.truncate = true;
            label.maxWidth = 100;
            label.tooltipText = "{category}";

        let axisTooltip = categoryAxis.tooltip;
            axisTooltip.background.strokeWidth = 0;
            axisTooltip.background.cornerRadius = 3;
            axisTooltip.background.pointerLength = 0;
            axisTooltip.dx = 100;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 10;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "nama";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.locationX = 0.5;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
        }

        createSeries("n", "Negeri");
        createSeries("s", "Swasta");

        }); // end am4core.ready()

    }
</script>