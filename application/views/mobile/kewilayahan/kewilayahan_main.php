<?php
    $th_first = $this->uri->segment(5);
    // 
?>
<!-- maps -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Kewilayahan Kota Malang</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <div class="card-body"> -->
                                <div class="row" id="map" style="width: 100%; height: 600px; position: fixed;">
                    
                                </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">
    console.log('<?php print_r($data_main);?>');
    var statesData = JSON.parse('<?php print_r($data_main);?>');
    var map = L.map('map').setView([-7.9771206, 112.6340291], 12);

    console.log(statesData);

    // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    //  maxZoom: 18,
    //  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    //      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    //      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    //  id: 'mapbox.light'
    // }).addTo(map);

    L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
          subdomains: ['a', 'b', 'c']
        }).addTo( map );


    // control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Populasi Penduduk Kota Malang</h4>' +  (props ?
            '<b style=\'color:#000000;\'>' + props.name + '</b>': '<b style=\'color:#000000;\'>Arahkan Ke Area Kelurahan</b>');
    };

    info.addTo(map);


    // get color depending on population density value
    function getColor(d) {
        // return d > 1000 ? '#800026' :
        //      d > 500  ? '#BD0026' :
        //      d > 200  ? '#E31A1C' :
        //      d > 100  ? '#FC4E2A' :
        //      d > 50   ? '#FD8D3C' :
        //      d > 20   ? '#FEB24C' :
        //      d > 10   ? '#FED976' :
        //                  '#FFEDA0';

        var color;
        switch(d) {
          case "KECAMATAN LOWOKWARU":
            // code block
            color = "#2800F2";
            break;

          case "KECAMATAN BLIMBING":
            // code block
            color = "#F20018";
            break;

          case "KECAMATAN KLOJEN":
            // code block
            color = "#00EAF2";
            break;

          case "KECAMATAN KEDUNGKANDANG":
            // code block
            color = "#00F220";
            break;

          case "KECAMATAN SUKUN":
            // code block
            color = "#F2C100";
            break;

          default:
            // code block
            color = "#ACACAB";
        }

        return color;
    }

    function style(feature) {
        return {
            weight: 1,
            opacity: 1,
            color: 'black',
            fillOpacity: 0.5,
            fillColor: getColor(feature.properties.kecamatan)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 2,
            color: 'black',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

    geojson = L.geoJson(statesData, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(map);

    map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">US Census Bureau</a>');

</script>