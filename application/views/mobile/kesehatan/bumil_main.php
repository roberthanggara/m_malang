<?php
    $th_first = $this->uri->segment(5);
    //
    $str_title = "";
    if(isset($title)){
        $str_title = $title;
    }

    $default_date_start = $this->uri->segment(5);
    if(!$default_date_start){
        $default_date_start = date("Y-m-d");
    }

    $default_date_finish = $this->uri->segment(6);
    if(!$default_date_finish){
        $default_date_finish = date("Y-m-d");
    }
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Ibu Hamil (<?php print_r($str_title);?>)</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Filter Berdasarkan</label>
                                            <select class="form-control custom-select" name="type_filter" id="type_filter">
                                                <option value="get_data_by_age">Berdasarkan Umur</option>   
                                                <option value="get_data_by_tp">Berdasarkan Tanggal Perkiraan</option>
                                                <option value="get_data_by_hapl">Berdasarkan Hari Pertama Kehamilan</option>
                                                <option value="get_data_by_hamil">Berdasarkan Kehamilan</option>
                                                <option value="get_data_by_kelurahan">Berdasarkan Wilayah</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>tgl. Awal</label>
                                            <input type="date" class="form-control" name="tgl_start" id="tgl_start">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>tgl. Akhir</label>
                                            <input type="date" class="form-control" name="tgl_finish" id="tgl_finish">
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <button type="button" name="set_filter" id="set_filter" class="btn waves-effect waves-light btn-info">Siapkan Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Pilih Mode Chart</label>
                                            <br>
                                            <input type="radio" name="mode" id="mode1" value="0" checked="">
                                            <label for="mode1">Filter Berdasarkan Kecamatan</label>
                                            <br>

                                            <input type="radio" name="mode" id="mode2" value="1">
                                            <label for="mode2">Filter Berdasarkan Kelurahan</label>
                                            <br>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <select class="form-control custom-select" name="kec" id="kec">
                                            </select>
                                           <!--  <select class="form-control custom-select" id="filter_kec" name="filter_kec">
                                            </select> -->
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Warga Negara</label>
                                            <select class="form-control custom-select" id="filter_kategori" name="filter_kategori"></select>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <div class="row" id="out_body">
                                    
                                </div> -->

                                <div id="chartdiv"  style="width: 100%; height: 1000px;" ></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">
    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d"),
          am4core.color("#FFB6C1"),
          am4core.color("#FFE4B5")
        ];
      }
    }

    var data_wilayah = JSON.parse('<?php print_r($wilayah)?>');
    var data_kecamatan = JSON.parse('<?php print_r($kecamatan)?>');
    
    $(document).ready(function(){
        set_kecamatan();
        set_kelurahan();
    
    $("#tgl_start").val("<?php print_r($default_date_start);?>");
    $("#tgl_finish").val("<?php print_r($default_date_finish);?>");

        create_chart();

        mode_change();
    });

    $("#kec").change(function(){
        set_kelurahan();
        mode_change();
    }); 

    $("#kel").change(function(){
        
    }); 

    $("#type_filter").change(function(){
        var type_filter = $("#type_filter").val();
    });

    $("#set_filter").click(function(){
        var type_filter = $("#type_filter").val();

        set_next_page(type_filter);
    });

   function set_next_page(param){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var url = "";

        switch(param) {
          case "get_data_by_age":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_kelurahan":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_hamil":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_hapl":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_tp":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
          default:
            
        }

        window.location.href = url;
    }


    function set_kecamatan(){
        console.log(data_wilayah);
        var str_op_val = "";
        for(let elm in data_wilayah){
            if(elm != "1"){
                var nama_kecmatan = data_wilayah[elm].title.kecamatan;
                str_op_val += "<option value=\""+elm+"\">"+nama_kecmatan+"</option>";
            }
        }

        $("#kec").html(str_op_val);
    }

    function set_kelurahan(){
        var val_kec = $("#kec").val();

        var str_op_val = "";
        for(let elm in data_wilayah[val_kec].item){
            if(elm != "1"){
                // console.log(data_wilayah[val_kec].item[elm].idkelurahan);
                var nama_kelurahan = data_wilayah[val_kec].item[elm].kelurahan;
                var id_kelurahan = data_wilayah[val_kec].item[elm].idkelurahan;
                str_op_val += "<option value=\""+id_kelurahan+"\">"+nama_kelurahan+"</option>";
            }
        }

        $("#kel").html(str_op_val);
    }



    $("input[name='mode']").change(function(){
        mode_change();
    });


    function mode_change(){
        var mode = $("input[name='mode']:checked").val();
        var val_kec = $("#kec").val();
        var val_kel = $("#kel").val();
        if(mode == "1"){
            $("#grup_kec").removeAttr("hidden", true);
            $("#grup_kel").attr("hidden", true);
            // console.log(data_wilayah[val_kec].item_sum);
            // console.log(val_kec);
            create_chart(data_wilayah[val_kec].item_sum);
        }else{
            $("#grup_kec").attr("hidden", true);
            $("#grup_kel").attr("hidden", true);
            create_chart(data_kecamatan.item_sum);
            // console.log();
        }
        // console.log(mode);
    }

    function create_chart(data){
        am4core.ready(function() {

        // Themes begin
          am4core.useTheme(am4themes_myTheme);
          // am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdiv", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = data;

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "val";
        series.dataFields.category = "title";

        }); // end am4core.ready()
    }   
</script>