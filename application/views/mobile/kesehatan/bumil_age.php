<?php
    $th_first = $this->uri->segment(4);
    // 

    $str_title = "";
    if(isset($title)){
        $str_title = $title;
    }

    $default_date_start = $this->uri->segment(5);
    if(!$default_date_start){
        $default_date_start = date("Y-m-d");
    }

    $default_date_finish = $this->uri->segment(6);
    if(!$default_date_finish){
        $default_date_finish = date("Y-m-d");
    }

    // print_r($default_date_start);
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Ibu Hamil (<?php print_r($str_title);?>)</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Filter Berdasarkan</label>
                                            <select class="form-control custom-select" name="type_filter" id="type_filter">
                                                <option value="get_data_by_age">Berdasarkan Umur</option>   
                                                <option value="get_data_by_tp">Berdasarkan Tanggal Perkiraan</option>
                                                <option value="get_data_by_hapl">Berdasarkan Hari Pertama Kehamilan</option>
                                                <option value="get_data_by_hamil">Berdasarkan Kehamilan</option>
                                                <option value="get_data_by_kelurahan">Berdasarkan Wilayah</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>tgl. Awal</label>
                                            <input type="date" class="form-control" name="tgl_start" id="tgl_start">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>tgl. Akhir</label>
                                            <input type="date" class="form-control" name="tgl_finish" id="tgl_finish">
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <button type="button" name="set_filter" id="set_filter" class="btn waves-effect waves-light btn-info">Siapkan Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Pilih Mode Chart</label>
                                            <br>
                                            <input type="radio" name="mode" id="mode1" value="0" checked="">
                                            <label for="mode1">Filter Berdasarkan Kecamatan</label>
                                            <br>

                                            <input type="radio" name="mode" id="mode2" value="1">
                                            <label for="mode2">Filter Berdasarkan Kelurahan</label>
                                            <br>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group" id="grup_kec">
                                            <label>Kecamatan</label>
                                            <select class="form-control custom-select" name="kec" id="kec">
                                            </select>
                                           <!--  <select class="form-control custom-select" id="filter_kec" name="filter_kec">
                                            </select> -->
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Warga Negara</label>
                                            <select class="form-control custom-select" id="filter_kategori" name="filter_kategori"></select>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <div class="row" id="out_body">
                    
                                </div> -->

                                <div id="chartdiv"  style="width: 100%; height: 600px;" ></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">
    function am4themes_myTheme(target) {
      if (target instanceof am4core.ColorSet) {
        target.list = [ 
          am4core.color("#EC7063"),
          am4core.color("#AF7AC5"),
          am4core.color("#5DADE2"),
          am4core.color("#48C9B0"),
          am4core.color("#F4D03F"),
          am4core.color("#E67E22"),
          am4core.color("#A6ACAF"),
          am4core.color("#1A5276"),
          am4core.color("#142c6d"),
          am4core.color("#FFB6C1"),
          am4core.color("#FFE4B5")
        ];
      }
    }

   var data_wilayah = JSON.parse('<?php print_r($wilayah)?>');
    var data_item_all = JSON.parse('<?php print_r($item_all)?>');
    
    $(document).ready(function(){
        set_kecamatan();
    
        $("#tgl_start").val("<?php print_r($default_date_start);?>");
        $("#tgl_finish").val("<?php print_r($default_date_finish);?>");

        mode_change();
    });

    $("#kec").change(function(){
        mode_change();
    }); 

    $("#kel").change(function(){
        
    }); 

    $("#type_filter").change(function(){
        var type_filter = $("#type_filter").val();
    });

    $("#set_filter").click(function(){
        var type_filter = $("#type_filter").val();

        set_next_page(type_filter);
    });

    function set_kecamatan(){
        console.log(data_wilayah);
        var str_op_val = "";
        for(let elm in data_wilayah){
            if(elm != "1"){
                var nama_kecmatan = data_wilayah[elm].title.kecamatan;
                str_op_val += "<option value=\""+elm+"\">"+nama_kecmatan+"</option>";
            }
        }

        $("#kec").html(str_op_val);
    }

    function set_next_page(param){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var url = "";

        switch(param) {
          case "get_data_by_age":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_kelurahan":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_hamil":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_hapl":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
          case "get_data_by_tp":
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
            break;
                url = "<?= base_url();?>mobile/kesehatan/mainbumil/"+param+"/"+tgl_start+"/"+tgl_finish;
          default:
            
        }

        window.location.href = url;
    }



    $("input[name='mode']").change(function(){
        mode_change();
    });


    function mode_change(){
        var mode = $("input[name='mode']:checked").val();

        console.log(mode);
        var val_kec = $("#kec").val();
        
        if(mode == "1"){
            $("#grup_kec").removeAttr("hidden", true);

            create_chart(data_wilayah[val_kec].item);
        }else{
            $("#grup_kec").attr("hidden", true);

            create_chart(data_item_all);
        }
    }

    function create_chart(data){
        am4core.ready(function() {

        // Themes begin
          am4core.useTheme(am4themes_myTheme);
          // am4core.useTheme(am4themes_dark);
          am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart3D);

        // Add data
        chart.data = data;

        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "title";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.renderer.labels.template.hideOversized = false;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.tooltip.label.rotation = 270;
        categoryAxis.tooltip.label.horizontalCenter = "right";
        categoryAxis.tooltip.label.verticalCenter = "middle";

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Rentang Umur";
        valueAxis.title.fontWeight = "bold";

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = "val";
        series.dataFields.categoryX = "title";
        series.name = "Nilain";
        series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;
        columnTemplate.stroke = am4core.color("#FFFFFF");

        columnTemplate.adapter.add("fill", function(fill, target) {
          return chart.colors.getIndex(target.dataItem.index);
        })

        columnTemplate.adapter.add("stroke", function(stroke, target) {
          return chart.colors.getIndex(target.dataItem.index);
        })

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineY.strokeOpacity = 0;

        }); // end am4core.ready()
    }   

</script>