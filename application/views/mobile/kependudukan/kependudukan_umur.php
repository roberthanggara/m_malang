<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Kependudukan (Rentang Umur)</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tahun</label>
                                            <input class="form-control" id="th_first" name="th_first" value="<?php print_r(date("Y"))?>">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Data</label>
                                            <select class="form-control custom-select" id="filter_jenis" name="filter_jenis">
                                                <option value="lampid">Lahir Mati Pindah Dan Datang</option>
                                                <option value="agama">Penduduk Berdasarkan Agama</option>
                                                <option value="rekam_ktp">Penduduk Berdasarkan Perekaman KTP</option>
                                                <option value="kelompok_umur">Penduduk Berdasarkan Kelompok Umur</option>
                                                <option value="rekam_kk">Penduduk Berdasarkan Perekaman KK</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <button type="button" id="next" name="next" class="btn waves-effect waves-light btn-info">
                                                Siapkan Data
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <select class="form-control custom-select" id="filter_kec" name="filter_kec">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Bulan</label>
                                            <select class="form-control custom-select" id="filter_bln" name="filter_bln">
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Warga Negara</label>
                                            <select class="form-control custom-select" id="filter_kategori" name="filter_kategori"></select>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row" id="out_body">
                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


    <script>
        var data_json = JSON.parse('<?php print_r($data_json);?>');
        var data_label = JSON.parse('<?php print_r($label);?>');

        var data_option = JSON.parse('<?php print_r($data_option);?>');

        data_option

        var id_global;

        var list_kecamatan = [
                                    {"id":"blimbing", "ket":"KEC. Blimbing"},
                                    {"id":"kedung_kandang", "ket":"KEC. Kedungkandang"},
                                    {"id":"klojen", "ket":"KEC. Klojen"},
                                    {"id":"lowokwaru", "ket":"KEC. Lowokwaru"},
                                    {"id":"sukun", "ket":"KEC. Sukun"}
                                ];

        var list_main_jenis = {
            "lampid"        :"Lahir Mati Pindah Dan Datang",
            "agama"         :"Penduduk Berdasarkan Agama",
            "rekam_ktp"     :"Penduduk Berdasarkan Perekaman KTP",
            "kelompok_umur" :"Penduduk Berdasarkan Kelompok Umur",
            // "rekam_kk"      :"Penduduk Berdasarkan Perekaman KK"
        };


        var array_chart_div = [];
        var title_chart = [];

        var MONTHS = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

        var config = {};

        function am4themes_myTheme(target) {
          if (target instanceof am4core.ColorSet) {
            target.list = [
              am4core.color("#EC7063"),
              am4core.color("#AF7AC5"),
              am4core.color("#5DADE2"),
              am4core.color("#48C9B0"),
              am4core.color("#F4D03F"),
              am4core.color("#E67E22"),
              am4core.color("#A6ACAF"),
              am4core.color("#1A5276"),
              am4core.color("#142c6d")
            ];
          }
        }

        function create_jenis_list(){
            var str_option = "";
            for(let i in list_main_jenis){
                str_option += "<option value=\""+i+"\">"+list_main_jenis[i]+"</option>";
            }

            $("#filter_jenis").html(str_option);
        }

        $(document).ready(function(){
            set_val_th();

            create_jenis_list();
            create_month_list();
            
            create_list_kec();

            var id = $("#filter_kec").val();

            get_data(id);
        });


        $("#next").click(function(){
            var th_first = $("#th_first").val();
            var filter_jenis = $("#filter_jenis").val();
            
            if(filter_jenis == "lampid"){
                window.location.href = "<?php print_r(base_url());?>mobile/kependudukan/kependudukanmain/get_data/"+th_first;
            }else if(filter_jenis == "agama"){
                window.location.href = "<?php print_r(base_url());?>mobile/kependudukan/kependudukanmain/get_data_agama/"+th_first;
            }else if(filter_jenis == "rekam_ktp"){
                window.location.href = "<?php print_r(base_url());?>mobile/kependudukan/kependudukanmain/get_data_rekap/"+th_first;
            }else if(filter_jenis == "kelompok_umur"){
                window.location.href = "<?php print_r(base_url());?>mobile/kependudukan/kependudukanmain/get_data_umur/"+th_first;
            }else if(filter_jenis == "rekam_kk"){
                window.location.href = "<?php print_r(base_url());?>mobile/kependudukan/kependudukanmain/get_data/"+th_first;
            }
        });

        function create_jenis_list(){
            var str_option = "";
            for(let i in list_main_jenis){
                str_option += "<option value=\""+i+"\">"+list_main_jenis[i]+"</option>";
            }

            $("#filter_jenis").html(str_option);
        }

        function create_month_list(){
            var str_option = "";
            for(let i in data_json.kelompok_umur.kelompok_umur){
                var th = i.substr(0,4);
                var periode = parseInt(i.substr(4,2))-1;
                str_option += "<option value=\""+i+"\">"+MONTHS[periode]+"</option>";
            }

            $("#filter_bln").html(str_option);
        }

        function create_list_kec(){
            var str_option = "";
            for(let item in list_kecamatan){
                // console.log(list_kecamatan[item]);

                str_option += "<option value=\""+list_kecamatan[item].id+"\">"+list_kecamatan[item].ket+"</option>";
            }

            $("#filter_kec").html(str_option);
        }


        // function create_kategori_list(){
        //     var str_option = "";
        //     for(let i in list_jenis){
        //         str_option += "<option value=\""+i+"\">"+list_jenis[i]+"</option>";
        //     }

        //     $("#filter_kategori").html(str_option);
        // }


        $("#filter_bln").change(function(){
            var id = $("#filter_kec").val();
            get_data(id);
           
        });

        $("#filter_kec").change(function(){
            var id = $("#filter_kec").val();
            get_data(id);
           
        });

        function get_data(id){
            // console.log(id);
            // var filter_jenis        = $("#filter_jenis").val();

            var filter_bln          = $("#filter_bln").val();
            var filter_kecamatan    = id;

            id_global = id;

            create_canvas();      

            // console.log(data_json["kelompok_umur"]["kelompok_umur"][filter_bln][id_global]);
            create_chart("chart_umur", data_option[id], data_json["kelompok_umur"]["kelompok_umur"][filter_bln][id_global]);
            $("#myLargeModalLabel").html(data_option[id]);

            $("#surya").modal('show');
        }


        // function create_chart(div_chart, kec, data_main){
        //     console.log(data_main);
        //     am4core.ready(function() {

        //     // Themes begin
        //     am4core.useTheme(am4themes_animated);
        //     am4core.useTheme(am4themes_myTheme);
        //     // Themes end

        //     // Create chart instance
        //     var chart = am4core.create(div_chart, am4charts.XYChart3D);

        //     // Add data
        //     chart.data = data_main;

        //     // Create axes
        //     let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        //     categoryAxis.dataFields.category = "keterangan";
        //     categoryAxis.renderer.labels.template.rotation = 270;
        //     categoryAxis.renderer.labels.template.hideOversized = false;
        //     categoryAxis.renderer.minGridDistance = 20;
        //     categoryAxis.renderer.labels.template.horizontalCenter = "right";
        //     categoryAxis.renderer.labels.template.verticalCenter = "middle";
        //     categoryAxis.tooltip.label.rotation = 270;
        //     categoryAxis.tooltip.label.horizontalCenter = "right";
        //     categoryAxis.tooltip.label.verticalCenter = "middle";

        //     let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        //     valueAxis.title.text = kec;
        //     valueAxis.title.fontWeight = "bold";

        //     // Create series
        //     var series = chart.series.push(new am4charts.ColumnSeries3D());
        //     series.dataFields.valueY = "value";
        //     series.dataFields.categoryX = "keterangan";
        //     series.name = "value";
        //     series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        //     series.columns.template.fillOpacity = .8;

        //     var columnTemplate = series.columns.template;
        //     columnTemplate.strokeWidth = 2;
        //     columnTemplate.strokeOpacity = 1;
        //     columnTemplate.stroke = am4core.color("#FFFFFF");

        //     columnTemplate.adapter.add("fill", function(fill, target) {
        //       return chart.colors.getIndex(target.dataItem.index);
        //     })

        //     columnTemplate.adapter.add("stroke", function(stroke, target) {
        //       return chart.colors.getIndex(target.dataItem.index);
        //     })

        //     chart.cursor = new am4charts.XYCursor();
        //     chart.cursor.lineX.strokeOpacity = 0;
        //     chart.cursor.lineY.strokeOpacity = 0;

        //     }); // end am4core.ready()
        // }

        function create_chart(div_chart, kec, data_main){
            am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            am4core.useTheme(am4themes_myTheme);
            // Themes end

            // Create chart instance
            var chart = am4core.create(div_chart, am4charts.XYChart3D);

            // Add data
            chart.data = data_main;

            // Create axes
            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "keterangan";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.renderer.labels.template.hideOversized = false;
            categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.tooltip.label.rotation = 270;
            categoryAxis.tooltip.label.horizontalCenter = "right";
            categoryAxis.tooltip.label.verticalCenter = "middle";

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = kec;
            valueAxis.title.fontWeight = "bold";

            // Create series
            var series = chart.series.push(new am4charts.ColumnSeries3D());
            series.dataFields.valueY = "value";
            series.dataFields.categoryX = "keterangan";
            series.name = "value";
            series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            columnTemplate.stroke = am4core.color("#FFFFFF");

            columnTemplate.adapter.add("fill", function(fill, target) {
              return chart.colors.getIndex(target.dataItem.index);
            })

            columnTemplate.adapter.add("stroke", function(stroke, target) {
              return chart.colors.getIndex(target.dataItem.index);
            })

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.strokeOpacity = 0;
            chart.cursor.lineY.strokeOpacity = 0;

            }); // end am4core.ready()
        }


        function create_canvas(){
            var str_option = "<div class=\"col-md-12\"><div id=\"chart_umur\" style=\"width: 100%; height: 600px;\"></div></div>";
            // for(let i in list_kategori){
            //     str_option += "<div class=\"col-md-4\"><div id=\"chart_"+i+"\" style=\"width: 100%; height: 350px;\"></div></div>";
            // }

            // console.log(str_option);

            $("#out_body").html(str_option);
        }

        function set_val_th(){
            var th_first = "<?php print_r($th_first);?>";

            $("#th_first").val(th_first);
        }
    </script>