            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Anjungan Air Siap Minum PDAM KOTA MALANG</h3>
                </div>
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row el-element-overlay">
                    <div class="col-md-12">
                        <center><br>
                        <h2 class="card-title">Main Menu </h2>
                        <h6 class="card-subtitle m-b-20 text-muted">Dashboard Walikota</h6></center></div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/12-pendidikan2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>data/pendidikan/main"><i>PENDIDIKAN</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">PENDIDIKAN</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/15-dispendukcapil2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                            
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/kependudukan/Kependudukanmain/get_data/2019"><i>KEPENDUDUKAN</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">KEPENDUDUKAN</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/13-bp2d2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                            
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/pajak/bppdapi/index_data_realisasi"><i>PAJAK</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">PAJAK</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/6-kepegawaian2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                            
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/kepegawaian/kepegawaianmain/show_detail"><i>KEPEGAWAIAN</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">KEPEGAWAIAN</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/8-kewilayahan2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/kewilayahan/kewilayahanmain/kewilayahan"><i>KEWILAYAHAN</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">KEWILAYAHAN</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>          
                        </div>
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/9-kemasyarakatan2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/surat_miskin/suratmiskinmain"><i>KEMASYARAKATAN</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">KEMASYARAKATAN</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/14-pdam2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                            
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>show_report/pdammain/get_pelanggan_rekap"><i>PDAM</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">PDAM</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/5-keuangan2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                                
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/sipex/sipexmain/index_pendapatan_belanja"><i>EKONOMI</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">EKONOMI</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="el-card-item">
                                    <div class="el-card-avatar el-overlay-1"> <img src="<?php echo base_url("assets/images/16-kesehatan2.png"); ?>" alt="user" />
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                                <li><a class="btn default btn-outline" href="<?php print_r(base_url());?>mobile/kesehatan/mainbumil/get_data_by_kelurahan"><i>KESEHATAN</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 class="box-title">KESEHATAN</h3> <small>Dashboard Walikota Kota Malang</small>
                                        <br/> </div>
                                </div>
                            </div>
                        </div>
                   
                    </div>
                </div>            
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
                