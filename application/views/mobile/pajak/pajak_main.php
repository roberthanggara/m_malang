<?php
    $th_first = $this->uri->segment(5);
    // 
?>

            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Target dan Realisasi Pajak Daerah Kota Malang</h3>
                </div>
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Jenis Data</label>
                                            <select class="form-control custom-select" id="filter_jenis" name="filter_jenis">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row" id="out_body">
                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        var data_json_perbandingan = JSON.parse('<?php print_r($data_json_perbandingan);?>');
        
        $(document).ready(function(){
            
            create_canvas();
            create_jenis_list();

            set_graph_bulat(data_json_perbandingan[0]);
        });

        function currency(x){
          return x.toLocaleString('us-EG');
        }

        function am4themes_myTheme(target) {
          if (target instanceof am4core.ColorSet) {
            target.list = [
              // warna biru
              am4core.color("#00BFFF"),
                
              // warna orange
              am4core.color("#FF8C00"), 

              // warna hijau
              am4core.color("#32CD32")
            ];
          }
        }

        $("#filter_jenis").change(function(){
            create_canvas();

            var kec = $("#filter_jenis").val();
            set_graph_bulat(data_json_perbandingan[kec]);
        });

        function create_jenis_list(){
            var str_list_jenis = "";
            for(let item in data_main){
                str_list_jenis += "<option value=\""+item+"\">"+data_main[item].jenis_pajak+"</option>";
            }

            $("#filter_jenis").html(str_list_jenis);
        }


        function set_graph_bulat(data_val){
            am4core.ready(function() {

            // Themes begin
            
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("chart_main", am4charts.XYChart);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.data = data_val;

            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.dataFields.category = "title";
            categoryAxis.renderer.minGridDistance = 40;
            categoryAxis.fontSize = 11;
            categoryAxis.renderer.labels.template.dy = 5;


            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;
            valueAxis.renderer.minGridDistance = 30;
            valueAxis.renderer.baseGrid.disabled = true;


            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.categoryX = "title";
            series.dataFields.valueY = "val";
            series.columns.template.tooltipText = "{valueY.value}";
            series.columns.template.tooltipY = 0;
            series.columns.template.strokeOpacity = 0;

            // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
            series.columns.template.adapter.add("fill", function(fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
            });

            });
        }


        function create_canvas(){
            var str_option = "<div class=\"col-md-12\"><div id=\"chart_main\" style=\"width: 100%; height: 400px;\"></div></div>";
            

            $("#out_body").html(str_option);
        }
    </script>