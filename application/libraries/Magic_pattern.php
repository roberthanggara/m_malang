 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Magic_pattern {

    public function detected_symbol(){
    	$pattern = "/([^'|\"|;|&|@|#|`|#|\\$|<|>|\||\\\|\/|\n|=])/";
        return $pattern;
    }

    public function allowed_char(){
    	$pattern = "/[0-9|a-z]*/";
        return $pattern;
    }

    public function sample1(){
    	$pattern = "/([^a-zA-Z0-9-&;,.-_@()\s\n]|[\\$|<|>|\||\\\|\/|=])|(false)/";
    	return $pattern;
    }

    public function sample2(){
    	$pattern = "/['\";&@#`#\\$<>\|(\\\)(\/)(\n)(=)]/";
    	return $pattern;
    }

    public function allowed_char_general(){
    	$pattern = "/[^a-zA-Z0-9-&;,.-_@()\s\n]|[\\$|<|>|\||\\\|\/|=]/";
    	return $pattern;
    }

    public function allowed_char_general_with_enter(){
        $pattern = "/[^a-zA-Z0-9-&;,.-_@()\s\n]|[\\$|<|>|\||\\\|\/|=]/";
        return $pattern;
    }

    public function removing_script(){
        $pattern = "/<.+>/";
        return $pattern;
    }
}