 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_library {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function create_json($dir, $file_name, $str_content){
        $status_send = false;
        if(json_decode($str_content)){
            $fp = fopen($dir.$file_name, 'w');
            fwrite($fp, $str_content);
            fclose($fp);
            $status_send = true;
        }

        return $status_send;
    }

    public function read_json($dir, $file_name){
        $return_param = null;

        if(file_exists($dir.$file_name)){
            $str_content = file_get_contents($dir.$file_name);
            if($str_content){
                $return_param = $str_content;
            }
        }

        return $return_param;
    }

    function is_JSON($string){
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }
}