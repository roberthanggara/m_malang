 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_connection_url {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function url_test($url) {
        // $status = false;

        // $timeout = 300;
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        // $http_respond = curl_exec($ch);
        // $http_respond = trim(strip_tags($http_respond));
        // $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // // print_r($http_code);
        // if (($http_code == "200") || ($http_code == "302")) {
        //     $status = true;
        // }
        // curl_close($ch);

        // return $status;
        $ssl_config = array(
                    "ssl"=>array(
                        "verify_peer"=>false,
                        "verify_peer_name"=>false,
                    ),
                );

        stream_context_set_default($ssl_config);

        $response_status = false;
        if($url){
            $headers = get_headers($url);
            $statusCode = substr($headers[0], 9, 3);
            // print_r($statusCode);
            // print_r("url ok");
            if((int)$statusCode == 200){
                // print_r("url true");
                $response_status = true;
            }
        }

        return $response_status;
    }

    public function check_time_update($str_date_time){
        // not update
        $status_update = false;
        date_default_timezone_set("Asia/Bangkok");

        // $str_date_time = "2019-11-11 09:37:15";
        $time_old = date('Y-m-d H:i:s', strtotime($str_date_time));
        $jam = 3600*1;
        $time_now =  date('Y-m-d H:i:s');

        $time_update = date('Y-m-d H:i:s', strtotime($time_old)+$jam);
        if($time_update < $time_now){
            // update
            $status_update = true;
        }

        return $status_update;
    }
    
}